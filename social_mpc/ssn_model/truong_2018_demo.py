# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Chris Reinke, Alex Auternaud, Timothée Wintz
# chris.reinke@inria.fr
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import numpy as np
# from truong_2018_jax import SocialSpaceNav
from social_spaces import SocialSpaces
from social_mpc.ssn_model import group_detection as gd
import time
import cProfile
import pstats
import io
import jax.numpy as jnp
from matplotlib import pyplot as plt


# with cProfile.Profile() as pr:
world_size = [[-5., 5.], [-4., 4.]]
objects = np.array([[-1, -2, 5., 0.1], [2.5, 2., 0.1, 0.5], [-4., -2., 1.5, 0.1]])
map = np.zeros((100,150))
class config:
    step_meshes =  2  # set to 5 in the new version
          # 'dist_min_active_base_loss': 3.,
    group_detection_stride = 1.
          # 'group_idx': 3,
          # 'update_pose_goal': 20,
          # 'targeted_human': 13,
          # 'dist_robot_to_group': 2.,}
ssn = SocialSpaces(map_shape=map.shape, bbox=world_size)
# occupancy_map = ssn.world_occupancy_map()
# compute a velocity map based on the occupancy map
# velocity_map = ssn.occupancy_map_to_velocity_map(occupancy_map=occupancy_map)

init_t_0 = time.time()

# define the robot location
robot = (0., 0.5-3) # (x, y)

mu, sigma = 0, 0.15
np.random.seed(0)
noise = np.random.normal(mu, sigma, 1) # [0]  #

# define the people in the scence
persons = np.array([[0.5 + noise[0], 1.5 + noise[0], np.pi * 0.25, 0.0, 0.], # (x, y, direction, velocity, is_sitting)
            [2. + noise[0] , 1.5 + noise[0] , np.pi * 0.75, 0.0, 1.],
            [-2 + noise[0] , 0 + noise[0], np.pi * 0.55, 0.0, 0.],
            [-3.2 + noise[0] , 0 + noise[0], np.pi * 0.45, 0.0, 0.],
            # [0.5 + noise[0] , 2.5 + noise[0], np.pi * -0.25, 0.0, 0.],
            [2. + noise[0] , 2.5 + noise[0], np.pi * -0.75, 0.0, 0.]])

# persons = np.array([[-0.39072227,  1.55457687,  1.31277737+np.pi/2, 0., 0.],
#                     [-1.55022812,  1.89117813, -1.82647386+np.pi/2, 0., 0.]])
# print(np.sqrt(np.sum((persons[0, :2]-persons[1, :2])**2)))

# detect groups (optional)
groups = gd.identify_groups(persons, stride=1.0)
end_t = time.time() - init_t_0
print("detect groups computation time {:.3f}".format(end_t))

# compute the DSZ
init_t = time.time()
f_dsz = ssn.calc_dsz(persons, groups=groups)

extent = [ssn.bbox[0][0], ssn.bbox[0][1], ssn.bbox[1][0], ssn.bbox[1][1]]

#plt.imshow(f_dsz, origin='lower', extent=extent)
print(groups[1].center)


goal = ssn.calc_goal_pos(f_dsz, map, persons[groups[1].person_ids, :], groups[1].center)
print(goal)
#plt.plot(goal[0], goal[1])

#plt.show()
"""
    # define a velocity map based on the DSZ and combine it with the velocity map from the occupancy map
    velocity_map = np.minimum(velocity_map, 1.0 - f_dsz)
    end_t_1 = time.time() - init_t
    print("velocity_map computation time {:.3f}".format(end_t_1))
    # print(persons)
    # print(groups)
    # print(velocity_map.shape)
    # print(f_dsz.shape)

    # compute the goal position of the robot to join the group
    init_t = time.time()
    goal_group, A_out_group = ssn.calc_goal_pos(f_dsz, persons[np.asarray(groups[0][2:], dtype=np.int8), :])
    end_t_2 = time.time() - init_t
    print("goal position computation time {:.3f}".format(end_t_2))
    # compute the goal position to join the third person
    goal_p3, A_out_p3 = ssn.calc_goal_pos(f_dsz, persons[2, :].reshape((1, 5)))  # persons[np.asarray(groups[1][2:], dtype=np.int8), :])

    # calculate the paths from the robot to the different goal positions according to the velocity map
    init_t = time.time()
    path_group = ssn.calc_path(velocity_map, robot, goal_group)
    path_p3 = ssn.calc_path(velocity_map, robot, goal_p3)
    end_t_3 = time.time() - init_t
    print("path to goal position computation time {:.3f}".format(end_t_3))
    loop_t = time.time() - init_t_0
    print("loop step time {:.3f}".format(loop_t))

# visualize the paths and the environment
# ssn.plot_2d(f_dsz, robot=robot, persons=persons,
            # occupancy_map=occupancy_map, title='Joining the group')
# ssn.plot_2d(f_dsz, robot=robot, persons=persons, approaching_areas=A_out_group, goal=goal_group,
        # occupancy_map=occupancy_map, title='Joining the group')
ssn.plot_2d(f_dsz, robot=robot, persons=persons, approaching_areas=A_out_group, goal=goal_group,
        path=path_group, occupancy_map=occupancy_map, title='Joining the group')

ssn.plot_2d(f_dsz, robot=robot, persons=persons, approaching_areas=A_out_p3, goal=goal_p3,
            path=path_p3, occupancy_map=occupancy_map, title='Joining the single person')
# s = io.StringIO()
# ps = pstats.Stats(pr, stream=s).sort_stats('tottime')
# ps.print_stats()

# with open('profile.txt', 'w+') as f:
    # f.write(s.getvalue())

# pr.print_stats()
"""
