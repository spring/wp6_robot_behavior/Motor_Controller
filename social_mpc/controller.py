# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Alex Auternaud, Timothée Wintz
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

from multiprocessing import Process, Lock
from multiprocessing.managers import SharedMemoryManager
from multiprocessing import active_children
import atexit
import rospy
import time
import numpy as np
import yaml
import pkg_resources
import cv2
import os

from social_mpc.mpc import MPC
from social_mpc.path import PathPlanner
from social_mpc.goal import GoalFinder
from social_mpc.ssn_model.social_spaces import SocialSpaces, Group
from social_mpc.utils import constraint_angle, local_to_global, global_to_local
from social_mpc.config.config import ControllerConfig

from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped


class RobotController():
    def __init__(self, config_filename=None):
        self.controller_config = None

        self.shared_global_map = None
        self.shared_humans = None
        self.shared_groups = None

        self.goto_target_flag = False
        self.pan_target_id = None
        self.pan_target_flag = False
        self.pan_target_pose = None
        self.human_target_id = None
        self.relative_h_pose = None
        self.global_pose_h = None
        self.lock = Lock()

        self.path_planner_enabled = False
        self.path_planner = None
        self.path_planner_process = None


        self.goal_finder_enabled = False
        self.goal_finder = None
        self.goal_finder_process = None

        self.mpc = None
        self.is_init = False
        self.is_failure = False

        self.goto_goal = None
        self.pan_goal = None
        self.human_features = None
        self.last_human_features = None
        self.tangent_traj_vec = None
        self.check_object_fw_traj_offset = None
        self.thresh_local_map = None

        self.mode = None

        self.weights_description = None
        self.weights = None
        self.loss_coef = None
        self.loss_rad = None
        self.reg_parameter = None
        self.loss = None
        self.actions = None

        self.passed_config_loaded = None

        self.read_config(filename=config_filename)
        self.init_var()

        if self.path_planner_enabled or self.goal_finder_enabled:
            self.smm_enabled = True
        else:
            self.smm_enabled = False

        self._waypoints_pub = rospy.Publisher('/waypoints_path', Path, queue_size=10)


    def init_var(self):
        config = self.controller_config
        self.goto_goal = np.zeros(config.goto_target_dim)
        self.pan_goal = np.zeros(config.pan_target_dim)
        self.human_features = np.zeros(config.human_target_dim)
        self.last_human_features = np.zeros(config.human_target_dim)
        self.weights_description = config.weights_description
        self.weights = np.zeros(len(self.weights_description))
        self.loss_coef = np.zeros_like(config.loss_coef[0])
        self.loss_rad = np.zeros_like(config.loss_rad[0])
        self.reg_parameter = np.zeros_like(config.reg_parameter)
        self.loss = config.loss
        self.check_object_fw_traj_offset = config.check_object_fw_traj_offset
        self.thresh_local_map = 0.4

        if self.passed_config_loaded:
            rospy.loginfo('Desired configuration loaded')
        else:
            rospy.loginfo('Could not have been loaded the desired configuration, the basic config was loaded')

    def read_config(self, filename=None):
        if filename is None:
            filename = pkg_resources.resource_filename(
                'social_mpc', 'config/social_mpc.yaml')
        elif os.path.isfile(filename):
            self.passed_config_loaded = True
        else:
            filename = pkg_resources.resource_filename(
                'social_mpc', 'config/social_mpc.yaml')
        config = yaml.load(open(filename), Loader=yaml.FullLoader)
        self.controller_config = ControllerConfig(config)

        self.goal_finder_enabled = self.controller_config.goal_finder_enabled
        self.path_planner_enabled = self.controller_config.path_planner_enabled
        self.update_goals_enabled = self.controller_config.update_goals_enabled


    def init_from_state(self, state):
        self.init_buffers(state)
        if self.path_planner_enabled:
            self.init_path_planner(state)
        if self.goal_finder_enabled:
            self.init_goal_finder(state)
        self.init_mpc(state)
        self.is_init = True

    def is_waypoint_ready(self):
        if self.shared_waypoint is None:
            return False
        else:
            with self.lock:
                return self.shared_waypoint[0]

    def init_mpc(self, state):
        config = self.controller_config
        dim_config = {'goto_target_dim': config.goto_target_dim,
                      'human_target_dim': config.human_target_dim,
                      'pan_target_dim': config.pan_target_dim,
                      'cost_map_dim': state.local_map.shape[:2] + (2,),
                      'weights_dim': len(self.weights_description),
                      'loss_coef_dim': config.loss_coef.shape[1],
                      'loss_rad_dim': config.loss_rad.shape[1]}
        fw_horizon = int(config.fw_time/config.h)
        joints_lb = np.array([state.robot_config.min_pan_angle])
        joints_ub = np.array([state.robot_config.max_pan_angle])
        world_size = state.config.local_map_size

        self.mpc = MPC(h=config.h,
                       robot_config=state.robot_config,
                       dim_config=dim_config,
                       horizon=fw_horizon,
                       u_lb=np.array(config.u_lb),
                       u_ub=np.array(config.u_ub),
                       joints_lb=joints_lb,
                       joints_ub=joints_ub,
                       max_acceleration=np.array(
                           config.max_acceleration),
                       wall_avoidance_points=config.wall_avoidance_points,
                       max_iter=config.max_iter_optim,
                       cost_map_region=world_size)

        self.initial_action = np.zeros(self.mpc.actions_shape)
        # self.initial_action[:, -1] = 0.1 * config.u_ub[-1]
        self.actions = np.copy(self.initial_action)

    def init_buffers(self, state):
        if self.smm_enabled:
            self.smm = SharedMemoryManager()
            self.smm.start()
            list_type = self.smm.ShareableList

            gmap = state.global_map
            self.shm_global_map = self.smm.SharedMemory(size=gmap.nbytes)
            self.shared_global_map = np.ndarray(shape=gmap.shape,
                                            dtype=gmap.dtype,
                                            buffer=self.shm_global_map.buf)

            if (state.humans_pose.shape[0] > 0):
                humans = np.zeros((state.humans_pose.shape[0], 7))  # x, y, orientation, lin vel, ang vel, group id, id
                self.shm_humans = self.smm.SharedMemory(size=humans.nbytes)
                self.shared_humans = np.ndarray(shape=humans.shape,
                                            dtype=humans.dtype,
                                            buffer=self.shm_humans.buf)

            if (state.groups_pose.shape[0] > 0):
                groups = np.zeros((state.groups_pose.shape[0], 3))  # x, y, id
                self.shm_groups = self.smm.SharedMemory(size=groups.nbytes)
                self.shared_groups = np.ndarray(shape=groups.shape,
                                            dtype=groups.dtype,
                                            buffer=self.shm_groups.buf)
                
            # Path computed by the Global Planner
            path = np.zeros((102, 2))  # x, y
            self.shm_path = self.smm.SharedMemory(size=path.nbytes)
            self.shared_path = np.ndarray(shape=path.shape,
                                        dtype=path.dtype,
                                        buffer=self.shm_path.buf)
        else:
            list_type = list

        self.flags = list_type([1])  # running
        self.shared_waypoint = list_type([False, 0.0, 0.0, 0.0])
        self.shared_target = list_type([False,  # is_ready
                                                     False,  # is_new
                                                     0.0,  # x
                                                     0.0,  # y
                                                     0.0])  # theta
        self.shared_robot_pose = list_type([0.0, 0.0, 0.0])
        self.shared_goto_target_human_id = list_type([False, 0])
        self.shared_goto_target_group_id = list_type([False, 0])


    def reset_shared_values(self):
        with self.lock:
            self.shared_waypoint[0] = False
            self.shared_waypoint[1] = 0.0
            self.shared_waypoint[2] = 0.0
            self.shared_waypoint[3] = 0.0
            self.shared_target[0] = False
            self.shared_target[1] = False
            self.shared_target[2] = 0.0
            self.shared_target[3] = 0.0
            self.shared_target[4] = 0.0
            self.shared_goto_target_human_id[0] = False
            self.shared_goto_target_human_id[1] = 0
            self.shared_goto_target_group_id[0] = False
            self.shared_goto_target_group_id[1] = 0
        self.controller_config.goto_target_id = -1
        self.controller_config.human_target_id = -1
        self.controller_config.goto_target_pose = np.zeros_like(self.controller_config.goto_target_pose)
        self.controller_config.pan_target_id = -1
        config = self.controller_config
        self.goto_goal = np.zeros(config.goto_target_dim)
        self.pan_goal = np.zeros(config.pan_target_dim)
        self.human_features = np.zeros(config.human_target_dim)
        self.last_human_features = np.zeros(config.human_target_dim)
        self.human_target_id = None
        self.goto_target_flag = False
        self.pan_target_flag = False
        self.pan_target_pose = None
        self.set_mode()
        self.set_weights()


    def on_subprocess_exit(self, name):
        if (self.flags[0] != 0):
            rospy.logerr("Subprocess {0} crashed. Closing".format())
        self.close()


    def init_path_planner(self, state):
        config = self.controller_config
        self.path_planner = PathPlanner(
            state_config=state.config,
            robot_config=state.robot_config,
            path_loop_time=config.path_loop_time,
            max_d=config.waypoint_distance,
            max_d_orientation=config.orientation_distance_threshold,
                                        )

        self.path_planner_process = Process(target=self.path_planner.run,
                                            args=(self.shared_global_map,
                                                  self.shared_waypoint,
                                                  self.shared_target,
                                                  self.shared_robot_pose,
                                                  self.flags,
                                                  self.lock,
                                                  self.shared_path))
        self.path_planner_process.start()
        # atexit.register(lambda x: self.on_subprocess_exit("Path planner"), self.path_planner_process)

    def update_shared_memory(self, state):
        if self.shared_global_map is None:
            self.init_buffers(state)
        if self.shared_global_map.shape != state.global_map.shape:
            self.close()
            self.init_from_state(state)
        else:
            with self.lock:
                self.shared_global_map[:] = state.global_map
                for i in range(3):
                    pos = float(state.robot_pose[i])
                    self.shared_robot_pose[i] = pos
                if self.mode != 1:
                    self.shared_target[0] = True

                self.shared_humans[:, -1] = -1
                idx = np.where(state.humans_visible == 0)[0]
                n = len(idx)
                self.shared_humans[:n, -1] = state.humans_id[idx]
                self.shared_humans[:n, -2] = state.humans_group_id[idx]
                self.shared_humans[:n, :3] = state.humans_pose[idx, :]
                self.shared_humans[:n, 3:5] = state.humans_velocity[idx, :]

                self.shared_groups[:, -1] = -1
                idx = np.where(state.groups_visible == 0)[0]
                n = len(idx)
                self.shared_groups[:n, :2] = state.groups_pose[idx, :]
                self.shared_groups[:n, -1] = state.groups_id[idx]
                # if self.shared_goto_target_human_id[0]:
                #     if self.shared_goto_target_human_id[1] not in self.shared_humans[:, -1]:
                #         rospy.logwarn("Target human not in FOV, back to ground truth")
                #         idx = state.humans_id.tolist().index(self.shared_goto_target_human_id[1])
                #         self.shared_target[1] = True
                #         self.shared_target[2] = float(state.humans_pose[idx, 0])
                #         self.shared_target[3] = float(state.humans_pose[idx, 1])
                #         self.shared_target[4] = float(state.humans_pose[idx, 2])

    def set_target_pose(self, pose):
        with self.lock:
            self.shared_goto_target_human_id[0] = False  # disable joining a person
            self.shared_goto_target_group_id[0] = False  # disable joining a group
            self.shared_target[1] = True
            self.shared_target[2] = float(pose[0])
            self.shared_target[3] = float(pose[1])
            self.shared_target[4] = float(pose[2])

    def set_target_human_id(self, human_id):
        with self.lock:
            self.shared_goto_target_human_id[0] = True
            self.shared_goto_target_human_id[1] = human_id

    def set_target_group_id(self, group_id):
        with self.lock:
            self.shared_goto_target_group_id[0] = True
            self.shared_goto_target_group_id[1] = group_id

    def set_goto_target(self, state, target, group=False):
        self.goto_target_flag = False
        self.is_failure = False
        if isinstance(target, int):
            if target >= 0:
                if group and len(state.groups_id) > 0 and target in state.groups_id:
                    self.goto_target_flag = True
                    self.set_target_group_id(target)
                elif len(state.humans_id) > 0 and target in state.humans_id:
                    self.goto_target_flag = True
                    self.set_target_human_id(target)
                else:
                    rospy.logwarn('Target id does not exist for goto target')
            else:
                self.controller_config.goto_target_id = -1
                with self.lock:
                    self.shared_goto_target_human_id[0] = False
                    self.shared_goto_target_human_id[1] = 0
                    self.shared_goto_target_group_id[0] = False
                    self.shared_goto_target_group_id[1] = 0

        elif len(target) == self.controller_config.target_pose_goto_dim and (
            isinstance(target, np.ndarray) or all(
                isinstance(i, (int, float)) for i in target)):
            if target[-1]:
                self.goto_target_flag = True
                if target[-2]:
                    robot_pose = np.copy(state.robot_pose)
                    new_target = [*local_to_global(robot_pose, target[:2]), constraint_angle(state.robot_pose[-1] + target[2])]
                    self.set_target_pose(new_target)
                else:
                    self.set_target_pose(target[:-1])
        else:
            rospy.logdebug('Goto target not set')
            self.is_failure = True
            self.controller_config.goto_target_id = -1
            with self.lock:
                self.shared_goto_target_human_id[0] = False
                self.shared_goto_target_human_id[1] = 0
                self.shared_goto_target_group_id[0] = False
                self.shared_goto_target_group_id[1] = 0

    def set_human_target(self, state, target):
        self.human_target_id = None
        self.is_failure = False
        if isinstance(target, int):
            if target in state.humans_id:
                if target >= 0:
                    self.human_target_id = target
            else:
                rospy.logwarn('Target id does not exist for human target')
                self.is_failure = True
                self.controller_config.human_target_id = -1
        else:
            rospy.logdebug('Human target not valid')
            self.is_failure = True
            self.controller_config.human_target_id = -1

    def set_pan_target(self, state, target, group=False):
        self.pan_target_id = None
        self.pan_target_pose = None
        self.pan_target_flag = False
        self.is_failure = False
        if isinstance(target, int):
            if target >= 0:
                if group and len(state.groups_id) > 0 and target in state.groups_id:
                    self.pan_target_id = ('group', target)
                    self.pan_target_flag = True
                elif len(state.humans_id) > 0 and target in state.humans_id:
                    self.pan_target_id = ('human', target)
                    self.pan_target_flag = True
                else:
                    rospy.logwarn('Target id does not exist for pan target')
            else:
                self.controller_config.pan_target_id = -1
        elif len(target) == self.controller_config.target_pose_look_dim and (
            isinstance(target, np.ndarray) or all(
                isinstance(i, (int, float)) for i in target)):
            self.controller_config.pan_target_id = -1
            if target[-1]:
                self.pan_target_flag = True
                if target[-2]:
                    self.pan_target_pose = target[:2]
                else:
                    self.pan_target_pose = global_to_local(state.robot_pose, target[:2])
        else:
            rospy.logdebug('Human target not valid')
            self.is_failure = True
            self.controller_config.pan_target_id = -1

    def set_relative_h_pose(self, config):
        self.relative_h_pose = config.relative_h_pose
        while np.linalg.norm(self.relative_h_pose) < config.min_dist_to_robot:
            if self.relative_h_pose[0]< 0.:
                self.relative_h_pose[0] -= 0.05
            else:
                self.relative_h_pose[0] += 0.05
            if self.relative_h_pose[1]< 0.:
                self.relative_h_pose[1] -= 0.05
            else:
                self.relative_h_pose[1] += 0.05

    def set_targets(self, state, goto_target=None, human_target=None, pan_target=None):
        config = self.controller_config
        if goto_target is None:
            if config.goto_target_id > -1:
                goto_target = config.goto_target_id
            else:
                goto_target = config.goto_target_pose
        if human_target is None:
            human_target = config.human_target_id
        if pan_target is None:
            pan_target = config.pan_target_id
        if goto_target is not None:
            self.set_goto_target(state=state, target=goto_target)
        if human_target:
            self.set_human_target(state=state, target=human_target)
        if pan_target:
            self.set_pan_target(state=state, target=pan_target)
        self.set_relative_h_pose(config)

    def init_goal_finder(self, state):
        self.goal_finder = GoalFinder(
            state_config=state.config,
            controller_config=self.controller_config
            )
        self.goal_finder_process = Process(target=self.goal_finder.run,
                                           args=(self.shared_humans,
                                                 self.shared_groups,
                                                 self.shared_target,
                                                 self.shared_robot_pose,
                                                 self.shared_goto_target_human_id,
                                                 self.shared_goto_target_group_id,
                                                 self.shared_global_map,
                                                 self.flags,
                                                 self.lock))
        self.goal_finder_process.start()


    def compute_cost_map(self, state):
        self.local_map = state.local_map

    def position_to_px_coord(self, state, pos):
        scale = state.config.local_map_scale
        x_min = state.config.local_map_size[0][0]
        y_max = state.config.local_map_size[1][1]
        w = state.config.local_map_width
        h = state.config.local_map_height

        i = int((y_max - pos[1]) * scale)
        j = int((pos[0] - x_min) * scale)
        i = 0 if i < 0. else w - 1 if i >= w else i
        j = 0 if j < 0. else h - 1 if j >= h else j
        return [i, j]

    def px_to_position_ccord(self, state, pos):
        scale = state.config.local_map_scale
        x_min = state.config.local_map_size[0][0]
        x_max = state.config.local_map_size[0][1]
        y_min = state.config.local_map_size[1][0]
        y_max = state.config.local_map_size[1][1]

        i = y_max - pos[1]/scale
        j = x_min + pos[0]/scale
        i = x_min if i < x_min else x_max if i > x_max else i
        j = y_min if j < y_min else y_max if j > y_max else j
        return [i, j]

    def local_related_human_target(self, state, idx):
        flag = 1.
        pos_local, angle, vel = [0., 0.], 0., [0., 0.]
        if state.humans_visible[idx] == 0.:
            self.global_pose_h = state.humans_pose[idx, :]
            vel = state.humans_velocity[idx, :]
            alpha = abs(vel[0]) / self.controller_config.u_ub[-1]
            if self.tangent_traj_vec is None:
                self.tangent_traj_vec = np.copy(self.global_pose_h)
            self.tangent_traj_vec[:2] = self.global_pose_h[:2]
            if (self.global_pose_h[2] - self.tangent_traj_vec[2] > np.pi):
                new_theta = self.global_pose_h[2] - 2*np.pi
            elif (self.global_pose_h[2] - self.tangent_traj_vec[2] < -np.pi):
                new_theta = self.global_pose_h[2] + 2*np.pi
            else:
                new_theta = self.global_pose_h[2]
            self.tangent_traj_vec[2] = constraint_angle((1 - alpha)*self.tangent_traj_vec[2] + alpha*new_theta)

            pose = self.tangent_traj_vec.copy()
            relative_h_pose = -self.relative_h_pose

            human_local_pos_px = self.position_to_px_coord(state, global_to_local(state.robot_pose, pose[:2]))
            human_local_map_val = np.sum(self.local_map[human_local_pos_px[0], human_local_pos_px[1]])

            local_map_val = []
            delta = np.arange(- self.check_object_fw_traj_offset, self.controller_config.min_dist_to_robot + self.check_object_fw_traj_offset, 0.1)

            for i in range(len(delta)):
                fw_pos_global = local_to_global(pose, -self.relative_h_pose + [0., delta[i]])
                fw_pos_local = global_to_local(state.robot_pose, fw_pos_global)
                fw_pos_local_px = self.position_to_px_coord(state, fw_pos_local)
                local_map_val.append(np.sum(self.local_map[fw_pos_local_px[0], fw_pos_local_px[1]]))

            pos_global = local_to_global(pose, relative_h_pose)
            pos_local = global_to_local(state.robot_pose, pos_global)
            angle = constraint_angle(self.tangent_traj_vec[2] - state.robot_pose[2])

            sign = -1 if self.mode == 1 else 1 if self.mode == 2 else 0
            if ((local_map_val - human_local_map_val*self.thresh_local_map) > 0.).any() and vel.any():
                if self.mode == 1:
                    relative_h_pose = sign * np.array([0., self.controller_config.min_dist_to_robot])
                    diff_position_global = pose[:2] - state.robot_pose[:2]
                    diff_angle = constraint_angle(-np.arctan2(diff_position_global[0], diff_position_global[1]))
                    pos_global = local_to_global(np.asarray([*pose[:2], diff_angle]), relative_h_pose)
                    pos_local = global_to_local(state.robot_pose, pos_global)
                    angle = constraint_angle(-np.arctan2(pos_local[0], pos_local[1]))
                if self.mode == 2:
                    flag = 0.

        if state.humans_visible[idx] > 0. :
            pos_local = global_to_local(state.robot_pose, self.tangent_traj_vec[:2])
            vel = self.last_human_features[3:5]
            angle = constraint_angle(self.tangent_traj_vec[2] - state.robot_pose[2])

        return [pos_local, angle, vel, flag]

    def update_goals(self, state):
        if self.mode == 0 or self.mode == 2:
            with self.lock:
                shared_waypoint = np.array([*self.shared_waypoint])
            if shared_waypoint[0]:
                global_wpt = np.array([shared_waypoint[1],
                                       shared_waypoint[2]])
                local_wpt = global_to_local(state.robot_pose, global_wpt)
                wpt_angle = shared_waypoint[3] - state.robot_pose[-1]
                self.goto_goal[:] = [local_wpt[0], local_wpt[1], wpt_angle, 0., 0.,
                                     0, 0, 1]
                self.goto_goal[2] = constraint_angle(self.goto_goal[2])
                self.goto_goal[-2] = 1
                msg = Path()
                msg.header.frame_id = 'map'
                msg.header.stamp = rospy.Time.now()
                for (x, y) in self.shared_path:
                    if x != 0. and y != 0.:
                        pose = PoseStamped()
                        pose.pose.position.x = x
                        pose.pose.position.y = y
                        msg.poses.append(pose)
                self._waypoints_pub.publish(msg)

        if self.mode == 2 or self.mode ==1:
            self.human_features = np.zeros(
                self.controller_config.human_target_dim)
            humans_id = state.humans_id.tolist()
            if self.human_target_id in humans_id:
                idx = humans_id.index(self.human_target_id)
                pos_local, angle, vel, flag = self.local_related_human_target(state, idx)
                self.human_features[:] = [
                    *pos_local, angle, *vel, self.loss, 0, flag]
                self.human_features[-2] = 1
                self.last_human_features = self.human_features

        if self.mode == 3 or self.pan_target_flag:
            self.pan_goal = np.zeros(self.controller_config.pan_target_dim)
            if self.pan_target_id[0] == 'human':
                humans_id = state.humans_id.tolist()
                if self.pan_target_id[1] in humans_id:
                    idx = humans_id.index(self.pan_target_id[1])
                    if state.humans_visible[idx] == 0.:
                        global_pos = state.humans_pose[idx, :2]
                        vel = state.humans_velocity[idx, :]
                        pos = global_to_local(state.robot_pose, global_pos)
                        self.pan_goal[:] = [*pos, *vel, 1]
            if self.pan_target_id[0] == 'group':
                groups_id = state.groups_id.tolist()
                if self.pan_target_id[1] in groups_id:
                    idx = groups_id.index(self.pan_target_id[1])
                    if state.groups_visible[idx] == 0.:
                        global_pos = state.groups_pose[idx]
                        pos = global_to_local(state.robot_pose, global_pos)
                        self.pan_goal[:] = [*pos, 0, 0, 1]
            if self.pan_target_pose is not None:
                self.pan_goal[:] = [*self.pan_target_pose, 0, 0, 1]

    def set_mode(self):
        self.mode = None
        if self.goto_target_flag:
            if self.human_target_id is None:
                self.mode = 0  # join
            else:
                self.mode = 2  # escort
        elif self.human_target_id is not None:
                self.mode = 1  # follow
        elif self.pan_target_flag:
            self.mode = 3  # just look at
        else:
            rospy.logwarn('No mode was chosen, nothing to do')

    def set_weights(self):
        config = self.controller_config
        self.loss_coef = np.zeros_like(config.loss_coef[0])
        self.loss_rad = np.zeros_like(config.loss_rad[0])
        self.reg_parameter = np.zeros_like(config.reg_parameter)

        if self.mode is not None and self.mode != 3:
            for i, weight_name in enumerate(self.weights_description):
                self.weights[i] = config.__getattribute__(
                    weight_name)

            self.loss_coef = config.loss_coef[self.mode]
            self.loss_rad = config.loss_rad[self.mode]
            self.reg_parameter = config.reg_parameter.copy()
        elif self.pan_target_flag:
            # if only pan target id is set, load the join mode weights
            for i, weight_name in enumerate(self.weights_description):
                self.weights[i] = config.__getattribute__(
                    weight_name)

            self.loss_coef = config.loss_coef[0]
            self.loss_rad = config.loss_rad[0]
            self.reg_parameter = config.reg_parameter.copy()

    def do_step(self):
        zero_value_params = np.any(self.loss_coef) or np.any(self.loss_rad) or np.any(self.reg_parameter)
        zero_value_goals = np.any(self.goto_goal) or np.any(self.pan_goal) or np.any(self.human_features)
        if self.mode is None or not zero_value_params or not zero_value_goals:
            return False
        else:
            return True

    def step(self, state):
        if not self.is_init:
            self.init_from_state(state)
            self.set_targets(state=state)
            self.set_mode()
            self.set_weights()
        self.compute_cost_map(state)

        if self.update_goals_enabled or self.path_planner_enabled:
            self.update_shared_memory(state)

        if self.update_goals_enabled:
            self.update_goals(state)

        if self.do_step():
            if not self.pan_target_flag and not self.goto_target_flag and (self.human_target_id == -1 or self.human_target_id == None):
                self.reg_parameter[-1] = 100000
            self.actions = self.mpc.step(state=state.joint_angles,
                                         actions=self.actions + self.initial_action,
                                         weights=self.weights,
                                         reg_parameter=self.reg_parameter,
                                         loss_coef=self.loss_coef,
                                         loss_rad=self.loss_rad,
                                         cost_map=self.local_map[:, :, :-1],
                                         goto_goal=self.goto_goal,
                                         pan_goal=self.pan_goal,
                                         human_features=None) #self.human_features)
        else:
            self.actions = np.copy(self.initial_action)

    def close(self):
        active = active_children()
        rospy.loginfo(f'Active Children before closing: {len(active)}') 
        with self.lock:
            self.flags[0] = 0
        if self.path_planner_enabled:
            self.path_planner_process.join()
        if self.goal_finder_enabled:
            self.goal_finder_process.join()
        if self.smm_enabled:
            self.smm.shutdown()
        self.is_init = False
        active = active_children()
        rospy.loginfo(f'Active Children at the end of closing: {len(active)}')
        rospy.loginfo('RobotController closed')
