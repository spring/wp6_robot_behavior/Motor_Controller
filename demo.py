#!/usr/bin/env python
# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Alex Auternaud, Timothée Wintz
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import gym
import time
import numpy as np
import yaml
import os
import logging
import pkg_resources
from datetime import datetime
from shutil import copyfile

from bash_args import *

from social_mpc.controller import RobotController
from social_mpc.config.config import RunDemoConfig
from social_mpc.plots import MPCPlotter
from social_mpc.ssn_model.truong_2018 import SocialSpaceNav
from social_mpc import utils

logger = logging.getLogger("demo")
logging.basicConfig()


def autocorr1(x,lags):
    '''np.corrcoef, partial'''

    corr=[1. if l==0 else np.corrcoef(x[l:],x[:-l])[0][1] for l in lags]
    return np.array(corr)

def demo():
    save_path = pkg_resources.resource_filename(
        'results', '/data/config/')
    mpc_module_path = pkg_resources.resource_filename(
        'social_mpc', '/config/')
    sim2d_module_path = pkg_resources.resource_filename(
        'sim2d', '/config/')

    try:
        run_demo_filename =  pkg_resources.resource_filename(
            'results', '/data/config/social_mpc/'+ CONFIG_NAME + '_run_demo.yaml')
        run_demo_config = yaml.load(open(run_demo_filename), Loader=yaml.FullLoader)
    except:
        run_demo_filename = pkg_resources.resource_filename(
            'social_mpc', 'config/run_demo.yaml')
        run_demo_config = yaml.load(open(run_demo_filename), Loader=yaml.FullLoader)

    try:
        controller_config_filename =  pkg_resources.resource_filename(
            'results', '/data/config/social_mpc/'+ CONFIG_NAME + '_social_mpc.yaml')
        controller_config = yaml.load(open(controller_config_filename), Loader=yaml.FullLoader)
    except:
        controller_config_filename = None
    config_path =  pkg_resources.resource_filename('results', '/data/config/sim2d/'+ CONFIG_NAME)

    run_demo_config = RunDemoConfig(run_demo_config)
    logger.setLevel(level=run_demo_config.logging_mode)
    if not VERBOSE:
        logging.disable()

    env = gym.make('HumanRobot2D-v0', gui=GUI, config_path=config_path)
    env.reset()

    
    controller = RobotController(config_filename=controller_config_filename)
    state, _, _, info = env.step([0., 0., 0.])
    controller.step(state)

    plot_render = run_demo_config.plot_render and GUI and PLOT
    max_steps = run_demo_config.max_steps

    if plot_render:
        mpc_plotter = MPCPlotter(direction_norm=0.5,
                                 local_map_size=controller.mpc.cost_map_region,
                                 global_map_size=state.config.global_map_size,
                                 local_map_scale=state.config.local_map_scale,
                                 global_map_scale=state.config.global_map_scale,
                                 wall_avoidance_points=controller.controller_config.wall_avoidance_points,
                                 max_step=max_steps)

    np_mean = 0.  # np.zeros(max_steps)
    np_std = 0.  # np.zeros(max_steps)
    norm_mean = 0.
    fw_loss_mean = 0.
    np_time = np.zeros(max_steps)  # x, y, theta, joint_angles
    np_robot_pose = np.zeros((max_steps, 3 + len(state.joint_angles)))
    np_values = np.zeros((max_steps, 3))
    np_metrics = np.zeros((max_steps, 4, 3))  # np.zeros((4, 3))  # smoothness1, 2, 3, work
    np_dist = np.zeros(max_steps)
    np_norm = np.ones((max_steps, 4))  # x, y, ang, pan error
    np_norm_escorted = np.zeros((max_steps, 3))
    np_human_pose_global = np.zeros((max_steps, 3))  # x, y, ang
    np_fw_loss = np.ones(max_steps)
    range_valid_thresh = controller.mpc.horizon - 1
    save_data = run_demo_config.save_data and SAVE
    end_time = run_demo_config.end_time
    h_idx_to_save = run_demo_config.h_idx_to_save
    t_loop = time.time()
    terminate = 0.

    for i in range(max_steps):

        tt = time.time()
        if i > 0:
            action_idx = 1
        else:
            action_idx = 0
        action = controller.actions[action_idx]
        logger.info(' dpan, omega, v : {}'.format(controller.actions[action_idx]))
        state, _, done, info = env.step(controller.actions[action_idx].tolist())
        logger.debug(' env step : {:.4}'.format(time.time() - tt))

        t = time.time()
        controller.step(state)
        logger.debug("elapsed for mpc : {:.4}".format(time.time() - t))
        np_time[i] = time.time() - t_loop
        t_loop = time.time()

        t = time.time()
        if plot_render:
            human_features = controller.human_features
            with controller.lock:
                target = None
                if controller.shared_target[0]:
                    target = np.zeros(3)
                    target[0] = controller.shared_target[2]
                    target[1] = controller.shared_target[3]
                    target[2] = controller.shared_target[4]
                waypoint = None
                if controller.shared_waypoint[0]:
                    waypoint = np.zeros(3)
                    waypoint[0] = controller.shared_waypoint[1]
                    waypoint[1] = controller.shared_waypoint[2]
                    waypoint[2] = controller.shared_waypoint[3]
                robot_pose = np.zeros(3)
                robot_pose[0] = controller.shared_robot_pose[0]
                robot_pose[1] = controller.shared_robot_pose[1]
                robot_pose[2] = controller.shared_robot_pose[2]

            mpc_plotter.update(mpc_fw_loss=controller.mpc.fw_loss,
                               joint_angles=state.joint_angles,
                               local_map=controller.local_map[:, :, :-1],
                               global_map=state.global_map[:, :, 0],
                               base_goal=controller.goto_goal,
                               human_features=human_features,
                               pan_goal=controller.pan_goal,
                               tangent_traj_vec=controller.tangent_traj_vec,
                               target=target,
                               waypoint=waypoint,
                               robot_pose=robot_pose)
        logger.debug("elapsed for plot render : {:.4}".format(time.time() - t))

        t = time.time()
        np_robot_pose[i, :len(state.robot_pose)] = state.robot_pose
        np_robot_pose[i, len(state.robot_pose):len(state.robot_pose) + len(state.joint_angles)] = state.joint_angles
        np_values[i, :] = action
        np_fw_loss[i] = controller.mpc.fw_loss
        if i > 0:
            np_dist[i] = np_time[i]*abs(action[-1])
            logger.info(' dist : {:.4}'.format(np.sum(np_dist)))
            np_mean = np.mean(np_time[1:i+1])
            np_std = np.std(np_time[1:i+1])
            # stats = np_time[1:i+1]
            # stats = stats[stats > h]
            # if len(stats) >= 1:
            #     print('max : {:.4}'.format(np.max(stats)))
            #     print('len : {}, mean : {:.4}, std : {:.4}'.format(len(stats), np.mean(stats), np.std(stats)))
            for k in range(len(action)):
                if np.any(np_values[:i+1, k]) and i > 1:
                    np_metrics[i, 0, k] = autocorr1(np_values[:i+1, k], [1])
                else:
                    np_metrics[i, 0, k] = 1.
                a_p1 = np.zeros_like(np_values[:i+1, k])
                a_p1[:-1] = np_values[1:i+1, k]
                a_n1 = np.zeros_like(np_values[:i+1, k])
                a_n1[1:] = np_values[:i, k]
                np_metrics[i, 1, k] = np.sum((a_n1[1:-1]-2*np_values[1:i, k]+a_p1[1:-1])**2)/len(np_values[:i+1, k])
                diff = np_values[:i, k] - a_p1[:-1]
                if np.any(diff):
                    np_metrics[i, 2, k] = np.std(diff)/np.mean(abs(diff))
                else:
                    np_metrics[i, 2, k] = 0.
                np_metrics[i, 3, k] = np.sum(abs((a_n1[1:-1]-2*np_values[1:i, k]+a_p1[1:-1])*(a_p1[1:-1]-np_values[1:i, k])))
        logger.debug(' metrics : {}'.format(np_metrics[i, :, :]))
        pan_error = 0.
        if controller.pan_goal[-1]:
            pan_error = -np.arctan2(controller.pan_goal[0], controller.pan_goal[1]) - state.joint_angles
        if controller.mode == 0 or controller.mode == 2:
            base_ang_error = 0.
            if controller.goto_goal[-2]:
                base_ang_error = controller.goto_goal[2]
            np_norm[i, :] = np.hstack((controller.goto_goal[:2], base_ang_error, pan_error))
        if controller.mode == 1:
            np_human_pose_global[i, :] = controller.global_pose_h
            base_ang_error = 0.
            if controller.human_features[-2]:
                base_ang_error = controller.human_features[2]
            np_norm[i, :] = np.hstack((controller.human_features[:2], base_ang_error, pan_error))
        if controller.mode == 2:
            np_human_pose_global[i, :] = controller.global_pose_h
            np_norm_escorted[i, :] = controller.human_features[:3]
        if controller.mode == 0:
            np_human_pose_global[i, :] = state.humans_pose[h_idx_to_save, :]
        if i >= range_valid_thresh:
            norm_mean = np.mean(np_norm[i-range_valid_thresh:i+1, :])
            fw_loss_mean = np.mean(np_fw_loss[i-range_valid_thresh:i+1])
        logger.info(' norm : {}, mean norm last horizon: {:.4}, min : {}'.format(np_norm[i, :], norm_mean, np.min(np_norm, axis=0)))
        logger.info(' fw_loss : {:.4}, mean fw_loss last horizon : {:.4}'.format(np_fw_loss[i], fw_loss_mean))
        if np.sum(np_time[1:i+1]) > end_time:
            logger.info('------finished------')
            terminate = 1.
        logger.debug(' metrics time : {:.4}'.format(time.time() - t))
        logger.info('----------------loop time-------------- : {:.4}, Controller step : {}, mean : {:.4}, std : {:.4}, total time : {:.4}'.format(np_time[i], i, np_mean, np_std, np.sum(np_time[1:i+1])))
        if done or info['max_steps'] or not info['gym_running'] or terminate:
            if terminate:
                if save_data :
                    dt_string = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
                    np.save('results/data/np_arrays/' + dt_string + '_np_values.npy', np_values[:i+1])
                    np.save('results/data/np_arrays/' + dt_string + '_np_human_final_pose_global.npy', state.humans_pose)
                    np.save('results/data/np_arrays/' + dt_string + '_np_robot_pose.npy', np_robot_pose[:i+1, :])
                    np.save('results/data/np_arrays/' + dt_string + '_np_metrics.npy', np_metrics[:i+1, :, :])
                    np.save('results/data/np_arrays/' + dt_string + '_np_fw_loss.npy', np_fw_loss[:i+1])
                    np.save('results/data/np_arrays/' + dt_string + '_np_norm.npy', np_norm[:i+1])
                    np.save('results/data/np_arrays/' + dt_string + '_np_dist.npy', np_dist[:i+1])
                    np.save('results/data/np_arrays/' + dt_string + '_np_time.npy', np_time[:i+1])
                    np.savez(file='results/data/np_arrays/global_map.npz',
                             global_map_size=state.config.global_map_size,
                             global_map_scale=state.config.global_map_scale,
                             global_map=state.global_map[:, :, 0])
                    if controller.mode == 2:
                        np.save('results/data/np_arrays/' + dt_string + '_np_norm_escorted.npy', np_norm_escorted[:i+1])
                    if np_human_pose_global.any():
                        np.save('results/data/np_arrays/' + dt_string + '_np_human_pose_global.npy', np_human_pose_global[:i+1])

                    ssn = SocialSpaceNav(
                        world_size=state.config.global_map_size,
                        objects=None,
                        config=controller.controller_config)

                    humans = np.column_stack((state.humans_pose, state.humans_velocity[:, 0], np.zeros(state.humans_pose.shape[0])))
                    humans[:, 2] = utils.robot_frame_to_ssn_frame_ang(humans[:, 2])
                    humans = np.vstack((humans, np.hstack((state.robot_pose[:2], utils.robot_frame_to_ssn_frame_ang(state.robot_pose[2]), state.robot_velocity[0], 0))))
                    groups = ssn.group_detection(humans, stride=ssn.stride)
                    f_dsz = ssn.calc_dsz(humans, delta_pose=ssn.delta_pose, groups=groups)
                    f_dsz = np.flip(f_dsz, axis=0)
                    np.savez(file='results/data/np_arrays/global_f_dsz.npz',
                             f_dsz=f_dsz,
                             x_mesh=ssn.x_mesh,
                             y_mesh=ssn.y_mesh)

                    dst_list = []
                    src_list = []
                    if CONFIG_NAME != 'basic_config':
                        sim2d_module_path = save_path + 'sim2d/' + CONFIG_NAME + '_'
                        mpc_module_path = save_path + 'social_mpc/' + CONFIG_NAME + '_'

                    dst_list.append(save_path + 'sim2d/' + dt_string + '_sim.yaml')
                    src_list.append(sim2d_module_path + 'sim.yaml')
                    dst_list.append(save_path + 'sim2d/' + dt_string + '_gym.yaml')
                    src_list.append(sim2d_module_path + 'gym.yaml')
                    dst_list.append(save_path + 'sim2d/' + dt_string + '_robot.yaml')
                    src_list.append(sim2d_module_path + 'robot.yaml')
                    dst_list.append(save_path + 'sim2d/' + dt_string + '_world.yaml')
                    src_list.append(sim2d_module_path + 'world.yaml')

                    dst_list.append(save_path + 'social_mpc/' + dt_string + '_social_mpc.yaml')
                    src_list.append(mpc_module_path + 'social_mpc.yaml')
                    dst_list.append(save_path + 'social_mpc/' + dt_string + '_run_demo.yaml')
                    src_list.append(mpc_module_path + 'run_demo.yaml')
                    for src, dst in zip(src_list, dst_list):
                        copyfile(src, dst)
            break

    if plot_render:
        mpc_plotter.close()

    controller.close()
    env.close()


if __name__ == "__main__":
    demo()
