# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Alex Auternaud, Timothée Wintz
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import skfmm
import numpy.ma as ma
import numpy as np
import time
from social_mpc import utils
from social_mpc.ssn_model.social_spaces import SocialSpaces


class PathPlanner():
    def __init__(self, state_config, robot_config, path_loop_time=None, max_d=1.0, max_d_orientation=1.0, max_target_angle=np.pi/5):
        self.state_config = state_config
        self.robot_config = robot_config
        self.max_d = max_d
        self.max_target_angle = max_target_angle
        self.max_d_orientation = max_d_orientation
        self.distance = None
        self.path_loop_time = path_loop_time
        world_size = state_config.global_map_size
        self.ssn = SocialSpaces(
            bbox=world_size,
            map_shape=(self.state_config.global_map_height,
                       self.state_config.global_map_width))
        self.waypoints_path = np.zeros((102, 2))
        self.offset_max_distance = 0.3 # in meter

    def position_to_px_coord(self, pos):
        """ Global map position (x, y) in meter to position 
        index in numpy array format """
        scale = self.state_config.global_map_scale
        x_min = self.state_config.global_map_size[0][0]
        y_max = self.state_config.global_map_size[1][1]
        y_min = self.state_config.global_map_size[1][0]

        i = (-y_min + pos[1]) * scale
        j = (pos[0] - x_min) * scale
        return int(i), int(j)

    def px_coord_grid(self):
        scale = self.state_config.global_map_scale
        x_min = self.state_config.global_map_size[0][0]
        y_min = self.state_config.global_map_size[1][0]

        x = [x_min + (j + 0.5) /
             scale for j in range(self.state_config.global_map_width)]
        y = [y_min + (i + 0.5) /
             scale for i in range(self.state_config.global_map_height)]
        return x, y

    def get_distance_map(self, global_map, big_number=100):
        t_i, t_j = self.position_to_px_coord(self.target)
        phi = ma.MaskedArray(np.ones(global_map.shape), mask=(global_map > 0))
        phi.data[t_i, t_j] = 0.0

        dx = 1/self.state_config.global_map_scale
        self.distance = skfmm.distance(phi, dx=dx)

    def get_next_waypoint(self, pos):
        x, y = self.ssn.x_coordinates, self.ssn.y_coordinates
        dx = 1/self.state_config.global_map_scale
        target_dist = np.linalg.norm(np.array(pos[:2]) - np.array(self.target[:2]))
        ret = utils.optimal_path_2d(
            self.distance, pos[:2],
            dx=dx,
            coords=(x, y),
            goal=np.array(self.target[:2]),
            max_d_orientation=self.max_d_orientation)
        
        if ret:
            xl, yl, vxl, vyl, dl = ret
        else :
            return []

        if len(xl) == 0 or len(yl) == 0 or len(vxl) == 0 or len(vyl) == 0:
            return []

        self.waypoints_path[0, :] =  [pos[0], pos[1]]
        for i, (x, y) in enumerate(zip(xl, yl)):
            self.waypoints_path[i+1, :] =  [x, y]
        self.waypoints_path[i+2, :] =  [self.target[0], self.target[1]]        
        waypoint_index = min(int(self.max_d / dx), len(dl)-1)  # get the index of the next waypoint at max_d with dx step
        if target_dist < self.max_d_orientation:  # if we are near the target, we only turn on ourself
            wpt_pos = [pos[0], pos[1], self.target[2]]
        else :
            angle=np.abs(utils.constraint_angle(np.arctan2(yl[waypoint_index] - pos[1], xl[waypoint_index] - pos[0]) - pos[2])) 
            #  if the angle to the next point is too big, also first turn on ourself to its direction
            if angle > self.max_target_angle:
                    wpt_pos = [pos[0], pos[1], np.arctan2(yl[waypoint_index] - pos[1], xl[waypoint_index] - pos[0])]
            else : 
                if target_dist < self.max_d: # we are near the target
                    wpt_pos = [self.target[0],
                            self.target[1],
                            np.arctan2(yl[-1] - pos[1], xl[-1] - pos[0])]
                else:  # nominal case else, point on shortest path with orientation towards target
                    wpt_pos = [xl[waypoint_index], yl[waypoint_index], np.arctan2(yl[waypoint_index] - pos[1], xl[waypoint_index] - pos[0])]
        return wpt_pos

    def run(self,
            shared_global_map,
            shared_waypoint,
            shared_target,
            shared_robot_pose,
            flags,
            lock,
            shared_path):
        last_time = time.time()

        while flags[0]:
            if self.path_loop_time:
                new_time = time.time()
                if (new_time - last_time) < self.path_loop_time:
                    time.sleep(self.path_loop_time - (new_time - last_time))
                last_time = time.time()

            with lock:
                sh_target = np.array([*shared_target])
                sh_robot_pose = np.array([*shared_robot_pose])
                sh_global_map = np.array([*shared_global_map])
                shared_path[:] = np.zeros_like(self.waypoints_path)
                self.waypoints_path = np.zeros_like(self.waypoints_path)
            if not sh_target[0]:  # not ready
                self.distance = None
                with lock:
                    shared_waypoint[0] = False
                continue
            if sh_target[1]:
                update_distance = True
                global_map = np.squeeze(sh_global_map)
                self.target = [sh_target[2],
                          sh_target[3], sh_target[4]]
            else:
                update_distance = False
            robot_pose = [sh_robot_pose[0],
                          sh_robot_pose[1],
                          sh_robot_pose[2]]
            if update_distance:
                try:
                    self.get_distance_map(global_map)
                except Exception as e:
                    print("could not compute distance (maybe target is in an obstacle?)")
                    with lock:
                        shared_waypoint[0] = False
                        continue

                with lock:
                    shared_target[1] = False
            elif self.distance is None:
                with lock:
                    shared_waypoint[0] = False
                    continue

            wpt = self.get_next_waypoint(robot_pose)
            
            if wpt:
                with lock:
                    shared_waypoint[0] = True
                    shared_waypoint[1] = float(wpt[0])
                    shared_waypoint[2] = float(wpt[1])
                    shared_waypoint[3] = float(wpt[2])
                    shared_path[:] = self.waypoints_path[:]
