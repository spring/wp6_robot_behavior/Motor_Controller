# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Alex Auternaud, Timothée Wintz
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import numpy as np
from scipy.interpolate import RectBivariateSpline
import jax.numpy as jnp
from jax import grad, jit, vmap, jacfwd, custom_jvp, partial


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

def find_nearest_sorted_idx(array,value):
    """if array already sorted"""
    idx = np.searchsorted(array, value, side="left")
    if (idx > 0).all() and ((idx == len(array)).all() or math.fabs(value - array[idx-1]) < math.fabs(value - array[idx])):
        return idx-1
    else:
        return idx

def local_to_global_np(robot_position, x):
    angle = rotmat_2d_np(-robot_position[:, -1])
    angle = np.moveaxis(angle, -1, 0)
    mul = np.einsum('ijk, ik -> ij', angle, x)
    return np.array(mul + robot_position[:, :2])


def local_to_global_jax(robot_position, x):
    angle = rotmat_2d_jax(robot_position[:, -1])
    y = vmap(vmpa_dot_jax, in_axes=(0, None))(jnp.moveaxis(angle, -1, 0), x)
    return jnp.array(y + robot_position[:, :2])


def vmpa_dot_jax(a, b):
    return a.dot(b)


def robot_frame_to_ssn_frame_ang(angle):
    return constraint_angle(angle + np.pi/2)


def ssn_to_robot_frame_ang(angle):
    return constraint_angle(angle - np.pi/2)


def box2d_frame_to_arrow_frame_ang(angle):
    return constraint_angle(- angle + np.pi/2)


def rotmat_2d(angle):
    return np.matrix([[np.cos(angle), -np.sin(angle)],
                      [np.sin(angle), np.cos(angle)]])

def rotmat_2d_jax(angle):
    return jnp.array([[jnp.cos(angle), -jnp.sin(angle)],
                      [jnp.sin(angle), jnp.cos(angle)]])


def rotmat_2d_np(angle):
    return np.array([[np.cos(angle), -np.sin(angle)],
                      [np.sin(angle), np.cos(angle)]])


def local_to_global(robot_position, x):
    angle = robot_position[-1]
    y = rotmat_2d(angle).dot(x) + robot_position[:2]
    return np.array(y).reshape(x.shape)


def global_to_local(robot_position, x):
    angle = robot_position[-1]
    y = rotmat_2d(-angle).dot(x - robot_position[:2])
    return np.array(y).reshape(x.shape)

def zero_2pi_angle(angles):
    return (2*np.pi + angles) * (angles < 0) + angles*(angles >= 0)

def constraint_angle(angle, min_value=-np.pi, max_value=np.pi):

    length = max_value - min_value

    # if angle > max_value:
    #     diff = angle - max_value
    #     new_angle = min_value + (diff % length)
    # elif angle < min_value:
    #     diff = min_value - angle
    #     new_angle = max_value - (diff % length)
    # else:
    #     new_angle = angle
    new_angle = np.where(angle > max_value, min_value +
                         ((angle - max_value) % length), angle)
    new_angle = np.where(angle < min_value, max_value -
                         ((min_value - angle) % length), new_angle)
    return new_angle


def optimal_path_2d(travel_time, starting_point, dx, coords, goal=None, max_d_orientation=None, N=100):
    """
    Find the optimal path from starting_point to the zero contour
    of travel_time. dx is the grid spacing
    Solve the equation x_t = - grad t / | grad t |
    """
    
    grad_t_y, grad_t_x = np.gradient(travel_time, dx)
    if isinstance(travel_time, np.ma.MaskedArray):
        grad_t_y[grad_t_y.mask] = 0.0
        grad_t_y = grad_t_y.data
        grad_t_x[grad_t_x.mask] = 0.0
        grad_t_x = grad_t_x.data

    d_interp = RectBivariateSpline(coords[1], coords[0],
                                   travel_time)
    gradx_interp = RectBivariateSpline(coords[1], coords[0],
                                       grad_t_x)
    grady_interp = RectBivariateSpline(coords[1], coords[0],
                                       grad_t_y)

    def get_velocity(position):
        """ return normalized velocity at pos """
        x, y = position
        vel = np.array([gradx_interp(y, x)[0][0],
                        grady_interp(y, x)[0][0]])
        return vel / np.linalg.norm(vel)

    def get_distance(position):
        x, y = position
        return d_interp(y, x)

    def euler_point_update(pos, ds):
        return pos - get_velocity(pos) * ds

    def runge_kutta(pos, ds):
        """ Fourth order Runge Kutta point update """
        k1 = ds * get_velocity(pos)
        k2 = ds * get_velocity(pos - k1/2.0)
        k3 = ds * get_velocity(pos - k2/2.0)
        k4 = ds * get_velocity(pos - k3)
        v = (k1 + 2*k2 + 2*k3 + k4)/6.0
        return pos - v, v

    # def sym(x, v):
    #     x = x[0], 0.5 * (coords[1][-1] + coords[1][0]) - x[1]
    #     v = v[0], -v[1]
    #     return x, v
    #
    # starting_point, _ = sym(starting_point, [0, 0])

    x, v = runge_kutta(starting_point, dx)
    xl, yl, vxl, vyl, dl = [], [], [], [], []
    starting_distance = get_distance(starting_point)



    for i in range(N):
        # xp, vp = sym(x, v)
        d = get_distance(x)[0][0]
        # if negative value d < 0, waypoint in an obstacle
        if d < 0:
            return False
        # goal is implicit defined in the travel_time map but we need to test it explicitly because we can not really trust d
        if ((goal is not None and max_d_orientation is not None) and
                (np.linalg.norm(goal - np.asarray(x)) < max_d_orientation)):
            if dl:
                break
        xl.append(x[0])
        yl.append(x[1])
        vxl.append(v[0])
        vyl.append(v[1])
        dl.append(d)
        x, v = runge_kutta(x, dx)

    return xl, yl, vxl, vyl, dl
