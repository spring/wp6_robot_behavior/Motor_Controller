#!/usr/bin/env python
# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Alex Auternaud
# alex.auternaud@inria.fr

import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
import matplotlib._color_data as mcd
from datetime import datetime

from social_mpc import utils

class SnaptoCursor(object):
    """
    Like Cursor but the crosshair snaps to the nearest x, y point.
    For simplicity, this assumes that *x* is sorted.
    """

    def __init__(self, ax, x, y):
        self.ax = ax
        self.lx = ax.axhline(color='k')  # the horiz line
        self.ly = ax.axvline(color='k')  # the vert line
        self.x = x
        self.y = y
        # text location in axes coords
        self.txt = ax.text(0.7, 0.9, '', transform=ax.transAxes)

    def mouse_move(self, event):
        if not event.inaxes:
            return

        x, y = event.xdata, event.ydata
        indx = min(np.searchsorted(self.x, x), len(self.x) - 1)
        x = self.x[indx]
        y = self.y[indx]
        # update the line positions
        self.lx.set_ydata(y)
        self.ly.set_xdata(x)

        self.txt.set_text('x=%1.2f, y=%1.2f' % (x, y))
        print('x=%1.2f, y=%1.2f' % (x, y))
        self.ax.figure.canvas.draw()

if __name__ == "__main__":
    __location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

    file_name = []
    file_name = [
                
                ]

    save_plot = 0
    max_len = 0
    time_annotate = True
    file_length = []

    for i in range(len(file_name)):
        file_length.append(np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_fw_loss.npy')).shape[0])
        if file_length[i] >= max_len:
            max_len = file_length[i]
    np_fw_loss = np.zeros((len(file_name), max_len))
    np_metrics = np.zeros((len(file_name), max_len, 4, 3))
    np_norm = np.zeros((len(file_name), max_len, 4))
    np_norm_escorted = np.zeros((len(file_name), max_len, 3))
    np_human_pose_global = np.zeros((len(file_name), max_len, 3))
    np_human_final_pose_global = np.zeros((len(file_name), 16, 3))
    np_values = np.zeros((len(file_name), max_len, 3))
    np_robot_pose = np.zeros((len(file_name), max_len, 4))
    np_dist = np.zeros((len(file_name), max_len))
    np_time = np.zeros((len(file_name), max_len))
    np_time_loop = np.zeros((len(file_name), max_len))
    global_map_data = np.load(os.path.join(__location__, 'data/np_arrays/global_map.npz'))
    global_f_dsz_data = np.load(os.path.join(__location__, 'data/np_arrays/global_f_dsz.npz'))
    dt_string = datetime.now().strftime("%d_%m_%Y_%H_%M")
    if len(file_name) > 10:
        color_choices = list(mcd.CSS4_COLORS.values())
    else:
        color_choices = list(dict(mcd.TABLEAU_COLORS).values())
    # color_choices = ['r', 'g', 'b', 'y']
    color = []

    for i in range(len(file_name)):
        # if i < 3:
        #     color.append(color_choices[0])
        # elif 3 <= i < 6:
        #     color.append(color_choices[1])
        # elif 6 <= i < 9:
        #     color.append(color_choices[2])
        # else:
        #     color.append(color_choices[3])
        color.append(color_choices[i])

        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_fw_loss.npy'))
        np_fw_loss[i, :np_array.shape[0]] = np_array
        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_metrics.npy'))
        np_metrics[i, :np_array.shape[0], :, :] = np_array
        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_norm.npy'))
        np_norm[i, :np_array.shape[0], :] = np_array
        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_values.npy'))
        np_values[i, :np_array.shape[0], :] = np_array
        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_robot_pose.npy'))
        np_robot_pose[i, :np_array.shape[0], :] = np_array
        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_dist.npy'))
        np_dist[i, :np_array.shape[0]] = np.cumsum(np_array)
        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_time.npy'))
        np_array[0] = 0.
        np_time[i, :np_array.shape[0]] = np.cumsum(np_array)
        np_time_loop[i, :np_array.shape[0]] = np_array  # np.convolve(np_array, np.ones(3)/3, mode='same')
        np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_human_final_pose_global.npy'))
        np_human_final_pose_global[i, :np_array.shape[0], :] = np_array
        try:
            np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_norm_escorted.npy'))
            np_norm_escorted[i, :np_array.shape[0], :] = np_array
        except:
            pass
        try:
            np_array = np.load(os.path.join(__location__, 'data/np_arrays/' + file_name[i] + '_np_human_pose_global.npy'))
            np_human_pose_global[i, :np_array.shape[0], :] = np_array
        except:
            pass
    # plots
    # losses
    # fig1, axs1 = plt.subplots(2, 2, figsize=(15.0, 15.0), sharex=True)
    # fig1.canvas.set_window_title('Plot Results Losses')
    # # fig1.suptitle('Plot Results Losses')
    # for i in range(len(file_name)):
    #     axs1[0, 0].plot(np_time[i, :file_length[i]], np_fw_loss[i, :file_length[i]], color=color[i], label=file_name[i])
    #     axs1[0, 1].plot(np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i]]), color=color[i], label=file_name[i])
    #     axs1[1, 0].plot(np_time[i, :file_length[i]], np_dist[i, :file_length[i]], color=color[i], label=file_name[i])
    #     axs1[1, 1].plot(np_time[i, :file_length[i]], np_time_loop[i, :file_length[i]], color=color[i], label=file_name[i])
    #     # snap_cursor1 = SnaptoCursor(axs1[0], np_time[i, :file_length[i]], np_fw_loss[i, :file_length[i]])
    #     # fig1.canvas.mpl_connect('motion_notify_event', snap_cursor1.mouse_move)
    #     # snap_cursor2 = SnaptoCursor(axs1[1], np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i]]))
    #     # fig1.canvas.mpl_connect('motion_notify_event', snap_cursor2.mouse_move)
    #     # snap_cursor3 = SnaptoCursor(axs1[2], np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i]]))
    #     # fig1.canvas.mpl_connect('motion_notify_event', snap_cursor3.mouse_move)
    #
    # axs1[0, 0].set_title('fw loss')
    # axs1[0, 1].set_title('L2 error')
    # axs1[1, 0].set_title('Distance run')
    # axs1[1, 1].set_title('Loop time')
    fig1 = plt.figure(figsize=(15.0, 15.0))
    fig1.canvas.set_window_title('Results Losses Plot')
    win_title = fig1.canvas.get_window_title().replace(" ", "_")
    gs = fig1.add_gridspec(4, 4)
    fig1_ax1 = fig1.add_subplot(gs[:2, :2])
    fig1_ax2 = fig1.add_subplot(gs[0, 2])
    fig1_ax3 = fig1.add_subplot(gs[0, 3])
    fig1_ax4 = fig1.add_subplot(gs[1, 2])
    fig1_ax5 = fig1.add_subplot(gs[1, 3])
    fig1_ax6 = fig1.add_subplot(gs[2:, :2])
    fig1_ax7 = fig1.add_subplot(gs[2:, 2:])
    fig1_axs = [fig1_ax1, fig1_ax2, fig1_ax3, fig1_ax4, fig1_ax5, fig1_ax6, fig1_ax7]
    for ax in fig1_axs:
        ax.grid(True)
        ax.axhline(y=0, color='k')
        ax.axvline(x=0, color='k')
    # fig1.suptitle('Plot Results Losses')
    for i in range(len(file_name)):
        fig1_ax1.plot(np_time[i, :file_length[i]], np_fw_loss[i, :file_length[i]], color=color[i], label=file_name[i])
        fig1_ax2.plot(np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i], 0]), color=color[i], label=file_name[i])
        fig1_ax3.plot(np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i], 1]), color=color[i], label=file_name[i])
        fig1_ax4.plot(np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i], 2]), color=color[i], label=file_name[i])
        fig1_ax5.plot(np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i], 3]), color=color[i], label=file_name[i])
        fig1_ax6.plot(np_time[i, :file_length[i]], np_dist[i, :file_length[i]], color=color[i], label=file_name[i])
        fig1_ax7.plot(np_time[i, :file_length[i]], np_time_loop[i, :file_length[i]], color=color[i], label=file_name[i])
        # snap_cursor1 = SnaptoCursor(axs1[0], np_time[i, :file_length[i]], np_fw_loss[i, :file_length[i]])
        # fig1.canvas.mpl_connect('motion_notify_event', snap_cursor1.mouse_move)
        # snap_cursor2 = SnaptoCursor(axs1[1], np_time[i, :file_length[i]], np.abs(np_norm[i, :file_length[i]])
        # fig1.canvas.mpl_connect('motion_notify_event', snap_cursor2.mouse_move)
        # snap_cursor3 = SnaptoCursor(axs1[2], np_time[i, :file_length[i]],np.abs(np_norm[i, :file_length[i]]))
        # fig1.canvas.mpl_connect('motion_notify_event', snap_cursor3.mouse_move)

    fig1_ax1.set_title('fw loss')
    fig1_ax1.set_xlabel('time (sec)')
    fig1_ax2.set_title('x error')
    fig1_ax2.set_xlabel('time (sec)')
    fig1_ax2.set_ylabel('error (m)')
    fig1_ax3.set_title('y error')
    fig1_ax3.set_xlabel('time (sec)')
    fig1_ax3.set_ylabel('error (m)')
    fig1_ax4.set_title('ang error')
    fig1_ax4.set_xlabel('time (sec)')
    fig1_ax4.set_ylabel('error (rad)')
    fig1_ax5.set_title('pan error')
    fig1_ax5.set_xlabel('time (sec)')
    fig1_ax5.set_ylabel('error (rad)')
    fig1_ax6.set_title('Distance run')
    fig1_ax6.set_xlabel('time (sec)')
    fig1_ax6.set_ylabel('distance (m)')
    fig1_ax7.set_title('Loop time')
    fig1_ax7.set_xlabel('time (sec)')
    fig1_ax7.set_ylabel('time (sec)')

    lines = []
    labels = []
    for ax in fig1.axes:
        lines_ax, labels_ax = ax.get_legend_handles_labels()
        for id, label in enumerate(labels_ax):
            if label not in labels:
                labels.append(label)
                lines.append(lines_ax[id])
    fig1.legend(lines, labels, loc = 'lower right')
    fig1.set_facecolor('w')
    plt.tight_layout()
    if save_plot:
        fig1.savefig(os.path.join(__location__, 'data/plots/' + dt_string + '_' + win_title + '.png'), dpi=600)

    # plt.show()

    # velocities
    fig2, axs2 = plt.subplots(3, figsize=(15.0, 15.0), sharex=True)
    fig2.canvas.set_window_title('Results Velocities Plot')
    win_title = fig2.canvas.get_window_title().replace(" ", "_")
    for ax in axs2:
        ax.grid(True)
        ax.axhline(y=0, color='k')
        ax.axvline(x=0, color='k')
    # fig2.suptitle('Plot Results Velocities')
    for i in range(len(file_name)):
        # axs2[0].plot(np_time[i, :file_length[i]], np_values[i, :file_length[i], 0], color=color[i], label=file_name[i])
        # axs2[1].plot(np_time[i, :file_length[i]], np_values[i, :file_length[i], 1], color=color[i], label=file_name[i])
        # axs2[2].plot(np_time[i, :file_length[i]], np_values[i, :file_length[i], 2], color=color[i], label=file_name[i])
        axs2[0].step(np_time[i, :file_length[i]], np_values[i, :file_length[i], 0], color=color[i], label=file_name[i], where='post')
        axs2[1].step(np_time[i, :file_length[i]], np_values[i, :file_length[i], 1], color=color[i], label=file_name[i], where='post')
        axs2[2].step(np_time[i, :file_length[i]], np_values[i, :file_length[i], 2], color=color[i], label=file_name[i], where='post')
    axs2[0].set_title('pan velocity')
    axs2[0].set_ylabel('velocity (rad/s)')
    axs2[1].set_title('angular velocity')
    axs2[1].set_ylabel('velocity (rad/s)')
    axs2[2].set_title('linear velocity')
    axs2[2].set_ylabel('velocity (m/s)')
    axs2[2].set_xlabel('time (sec)')

    lines = []
    labels = []
    for ax in fig2.axes:
        lines_ax, labels_ax = ax.get_legend_handles_labels()
        for id, label in enumerate(labels_ax):
            if label not in labels:
                labels.append(label)
                lines.append(lines_ax[id])
    fig2.legend(lines, labels, loc = 'lower right')
    fig2.set_facecolor('w')
    plt.tight_layout()
    if save_plot:
        fig2.savefig(os.path.join(__location__, 'data/plots/' + dt_string + '_' + win_title + '.png'), dpi=600)

    # plt.show()

    # metrics
    fig3, axs3 = plt.subplots(figsize=(15.0, 15.0), nrows=np_metrics.shape[2], ncols=1, sharex=True)
    fig3.canvas.set_window_title('Results Metrics Plot')
    win_title = fig3.canvas.get_window_title().replace(" ", "_")
    # fig3.suptitle('Plot Results Metrics')
    for row, sub_ax in enumerate(axs3, start=1):
        if row == 4:
            sub_ax.set_title("Work \n", fontsize=16)
        else:
            sub_ax.set_title("Smoothness %s \n" % row, fontsize=16)
        # Turn off axis lines and ticks of the big subplot
        # obs alpha is 0 in RGBA string!
        sub_ax.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
        # removes the white frame
        sub_ax._frameon = False

    c = 0
    for k in range(np_metrics.shape[2]):
        for i in range(np_metrics.shape[3]):
            c += 1
            ax = fig3.add_subplot(np_metrics.shape[2], np_metrics.shape[3], c)
            ax.grid(True)
            ax.axhline(y=0, color='k')
            ax.axvline(x=0, color='k')
            ax.set_xlabel('time (sec)')
            if i == 0:
                ax.set_title('dpan')
            if i == 1:
                ax.set_title('omega')
            if i == 2:
                ax.set_title('v')
            for idx in range(len(file_name)):
                ax.plot(np_time[idx, :file_length[idx]], np_metrics[idx, :file_length[idx], k, i], color=color[idx], label=file_name[idx])

    lines = []
    labels = []
    for ax in fig3.axes:
        lines_ax, labels_ax = ax.get_legend_handles_labels()
        for id, label in enumerate(labels_ax):
            if label not in labels:
                labels.append(label)
                lines.append(lines_ax[id])
    fig3.legend(lines, labels, loc = 'lower right')
    fig3.set_facecolor('w')
    plt.tight_layout()
    if save_plot:
        fig3.savefig(os.path.join(__location__, 'data/plots/' + dt_string + '_' + win_title + '.png'), dpi=600)

    if np_norm_escorted.any():
        fig4 = plt.figure(figsize=(15.0, 15.0))
        fig4.canvas.set_window_title('Escorted Human Errores Plot')
        win_title = fig4.canvas.get_window_title().replace(" ", "_")
        gs = fig4.add_gridspec(6, 6)
        fig4_ax1 = fig4.add_subplot(gs[:3, :3])
        fig4_ax2 = fig4.add_subplot(gs[:3, 3:])
        fig4_ax3 = fig4.add_subplot(gs[3:, 3:])
        fig4_ax4 = fig4.add_subplot(gs[3:, :3])
        fig4_axs = [fig4_ax1, fig4_ax2, fig4_ax3, fig4_ax4, ]
        for ax in fig4_axs:
            ax.grid(True)
            ax.axhline(y=0, color='k')
            ax.axvline(x=0, color='k')
        for i in range(len(file_name)):
            fig4_ax1.plot(np_time[i, :file_length[i]], np.abs(np_norm_escorted[i, :file_length[i], 0]), color=color[i], label=file_name[i])
            fig4_ax2.plot(np_time[i, :file_length[i]], np.abs(np_norm_escorted[i, :file_length[i], 1]), color=color[i], label=file_name[i])
            fig4_ax3.plot(np_time[i, :file_length[i]], np.linalg.norm(np.array([np.abs(np_norm_escorted[i, :file_length[i], 0]), np.abs(np_norm_escorted[i, :file_length[i], 1])]), axis=0), color=color[i], label=file_name[i])
            fig4_ax4.plot(np_time[i, :file_length[i]], np.abs(np_norm_escorted[i, :file_length[i], 2]), color=color[i], label=file_name[i])

        fig4_ax1.set_title('x error')
        fig4_ax1.set_xlabel('time (sec)')
        fig4_ax1.set_ylabel('error (m)')
        fig4_ax2.set_title('y error')
        fig4_ax2.set_xlabel('time (sec)')
        fig4_ax2.set_ylabel('error (m)')
        fig4_ax3.set_title('distance error from the escorted human target')
        fig4_ax3.set_xlabel('time (sec)')
        fig4_ax3.set_ylabel('distance (m)')
        fig4_ax4.set_title('ang error')
        fig4_ax4.set_xlabel('time (sec)')
        fig4_ax4.set_ylabel('error (rad)')


        lines = []
        labels = []
        for ax in fig4.axes:
            lines_ax, labels_ax = ax.get_legend_handles_labels()
            for id, label in enumerate(labels_ax):
                if label not in labels:
                    labels.append(label)
                    lines.append(lines_ax[id])
        fig4.legend(lines, labels, loc = 'lower right')
        fig4.set_facecolor('w')
        plt.tight_layout()
        if save_plot:
            fig4.savefig(os.path.join(__location__, 'data/plots/' + dt_string + '_' + win_title + '.png'), dpi=600)

    fig5 = plt.figure(figsize=(15.0, 15.0))
    fig5.canvas.set_window_title('Robot Trajectory and Pose Plot')
    win_title = fig5.canvas.get_window_title().replace(" ", "_")
    gs = fig5.add_gridspec(4, 3)
    fig5_ax1 = fig5.add_subplot(gs[0, 0])
    fig5_ax2 = fig5.add_subplot(gs[0, 1])
    fig5_ax3 = fig5.add_subplot(gs[0, 2])
    fig5_ax4 = fig5.add_subplot(gs[1:, :])
    fig5_axs = [fig5_ax1, fig5_ax2, fig5_ax3, fig5_ax4, ]
    max_dist_h = 0
    step_meter = 0.5

    for ax in fig5_axs:
        ax.grid(True)
        ax.axhline(y=0, color='k')
        ax.axvline(x=0, color='k')
    for i in range(len(file_name)):
        fig5_ax1.plot(np_time[i, :file_length[i]], np_robot_pose[i, :file_length[i], 2], color=color[i], label=file_name[i])
        fig5_ax2.plot(np_time[i, :file_length[i]], np_robot_pose[i, :file_length[i], 3], color=color[i], label=file_name[i])
        fig5_ax4.plot(np_robot_pose[i, :file_length[i], 0], np_robot_pose[i, :file_length[i], 1], color=color[i], label=file_name[i])
        idxs = utils.find_nearest_sorted_idx(np_dist[i, :][np_dist[i, :] > 0.], np.arange(0, max(np_dist[i, :]), step_meter))
        if time_annotate:
            for j in idxs:
                x, y = np_robot_pose[i, j, :2]
                circle = plt.Circle((x, y), radius=0.1, fill=False, color=color[i])
                fig5_ax4.add_patch(circle)
                label = fig5_ax4.annotate(round(np_time[i, j], 1), xy=(x, y), xytext=(x + 0.1, y + 0.15), fontsize=10, color=color[i])
        if np_human_pose_global.any():
            dist_h = np.linalg.norm(np_human_pose_global[i, :file_length[i], :2] - np_robot_pose[i, :file_length[i], :2], axis=1)
            if max(dist_h) > max_dist_h:
                max_dist_h = max(dist_h)
            fig5_ax3.plot(np_time[i, :file_length[i]], dist_h, color=color[i], label=file_name[i])
            fig5_ax4.plot(np_human_pose_global[i, :file_length[i], 0], np_human_pose_global[i, :file_length[i], 1], 'k', label='_'*i + 'Human Trajectory ')
            dist_h_cumul = np.cumsum(np.linalg.norm(np_human_pose_global[i, :file_length[i]-1, :2] - np_human_pose_global[i, 1:file_length[i], :2], axis=1))
            idxs = utils.find_nearest_sorted_idx(dist_h_cumul, np.arange(0, max(dist_h_cumul), step_meter))
            if time_annotate:
                for j in idxs:
                    x, y = np_human_pose_global[i, j, :2]
                    circle = plt.Circle((x, y), radius=0.1, fill=False, color='k')
                    fig5_ax4.add_patch(circle)
                    label = fig5_ax4.annotate(round(np_time[i, j], 1), xy=(x, y), xytext=(x + 0.1, y + 0.15), fontsize=10, color='k')

    if np_human_pose_global.any():
        if max_dist_h >= 0. and max_dist_h < 0.45:
            fig5_ax3.axhspan(0., max_dist_h, color='tab:orange', alpha=0.7, label='Intimate space')
        elif max_dist_h >= 0. and max_dist_h < 1.2:
            fig5_ax3.axhspan(0., 0.45, color='tab:orange', alpha=0.7, label='Intimate space')
            fig5_ax3.axhspan(0.45, max_dist_h, color='tab:orange', alpha=0.5, label='Personal space')
        elif max_dist_h >= 0. and max_dist_h < 3.6:
            fig5_ax3.axhspan(0., 0.45, color='tab:orange', alpha=0.7, label='Intimate space')
            fig5_ax3.axhspan(0.45, 1.2, color='tab:orange', alpha=0.5, label='Personal space')
            fig5_ax3.axhspan(1.2, max_dist_h, color='tab:orange', alpha=0.3, label='Social space')
        else:
            fig5_ax3.axhspan(0., 0.45, color='tab:orange', alpha=0.7, label='Intimate space')
            fig5_ax3.axhspan(0.45, 1.2, color='tab:orange', alpha=0.5, label='Personal space')
            fig5_ax3.axhspan(1.2, 3.6, color='tab:orange', alpha=0.3, label='Social space')
            fig5_ax3.axhspan(3.6, max_dist_h, color='tab:orange', alpha=0.1, label='Public space')

    [xmin, xmax], [ymin, ymax] = global_map_data['global_map_size']
    fig5_ax4.imshow(global_map_data['global_map'], extent=[xmin, xmax, ymin, ymax], cmap='binary')
    origin = 'lower'
    CS = fig5_ax4.contour(global_f_dsz_data['x_mesh'], global_f_dsz_data['y_mesh'], np.flip(global_f_dsz_data['f_dsz'], axis=0), 20, cmap=plt.cm.plasma, origin=origin)
    # cbar = fig5.colorbar(CS)
    # cbar.ax.set_ylabel('space model value')
    for idx, person in enumerate(np_human_final_pose_global[-1, :, :]):
        x_p, y_p, theta_p = person
        theta_p = utils.robot_frame_to_ssn_frame_ang(theta_p)
        e_p = matplotlib.patches.Ellipse((x_p, y_p), 0.15, 0.25,
                                 angle=theta_p*180/np.pi, linewidth=2, fill=False, zorder=2, edgecolor=plt.cm.plasma(0.))
        fig5_ax4.add_patch(e_p)
        label = fig5_ax4.annotate(idx, xy=(x_p, y_p), xytext=(x_p + 0.1, y_p + 0.15), fontsize=10, fontweight='bold', color=plt.cm.plasma(0.))
        plt.arrow(x_p, y_p, np.cos(theta_p) * 0.1, np.sin(theta_p) * 0.1, edgecolor=plt.cm.plasma(0.))


    fig5_ax1.set_title('Orientation Value')
    fig5_ax1.set_xlabel('time (sec)')
    fig5_ax1.set_ylabel('orientation (rad)')
    fig5_ax2.set_title('Pan Value')
    fig5_ax2.set_xlabel('time (sec)')
    fig5_ax2.set_ylabel('orientation (rad)')
    fig5_ax3.set_title('distance from the escorted human')
    fig5_ax3.set_xlabel('time (sec)')
    fig5_ax3.set_ylabel('distance (m)')
    fig5_ax4.set_title('Robot Trajectory')
    fig5_ax4.set_xlabel('x (m)')
    fig5_ax4.set_ylabel('y (m)')

    lines = []
    labels = []
    for ax in fig5.axes:
        lines_ax, labels_ax = ax.get_legend_handles_labels()
        for id, label in enumerate(labels_ax):
            if label not in labels:
                labels.append(label)
                lines.append(lines_ax[id])
    fig5.legend(lines, labels, loc = 'lower right')
    fig5.set_facecolor('w')
    plt.tight_layout()
    if save_plot:
        fig5.savefig(os.path.join(__location__, 'data/plots/' + dt_string + '_' + win_title + '.png'), dpi=600)

    plt.show()
