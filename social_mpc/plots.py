# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Alex Auternaud, Timothée Wintz
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

"""
Demonstrates common image analysis tools.

Many of the features demonstrated here are already provided by the ImageView
widget, but here we present a lower-level approach that provides finer control
over the user interface.
"""
# import initExample ## Add path to library (just for examples; you do not need this)

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import jax.numpy as jnp
from jax import jit
import sys
import time
import logging

from social_mpc import utils

logger = logging.getLogger("controller.plot")


OBJECT_COLORMAP = [
    (0.8, 0.9, 0.8, 0.),
    (0.8, 0.9, 0.8, 0.2),
    (0.8, 0.9, 0.8, 0.4),
    (0.8, 0.2, 0., 0.5),
    (0.8, 0.9, 0.8, 0.6),
    (0.8, 0.9, 0.8, 0.8),
    (0.8, 0.9, 0.8, 1.),
]

SOCIAL_COLORMAP = [
    (1., 0.6, 0.1, 0.),
    (1., 0.6, 0.1, 0.2),
    (1., 0.6, 0.1, 0.4),
    (0.8, 0.2, 0., 0.5),
    (1., 0.6, 0.1, 0.6),
    (1., 0.6, 0.1, 0.8),
    (1., 0.6, 0.1, 1.),
]


class MyArrowItem(pg.ArrowItem):
    def paint(self, p, *args):
        if 0. <= self.opts['angle'] <= 90. or -360. <= self.opts['angle'] <= -270.:
            p.translate(-self.boundingRect().bottomRight())
        if 90. < self.opts['angle'] <= 180. or -270. < self.opts['angle'] <= -180.:
            p.translate(-self.boundingRect().bottomLeft())
        if 180. < self.opts['angle'] <= 270. or -180. < self.opts['angle'] <= -90.:
            p.translate(-self.boundingRect().topLeft())
        if 270. <= self.opts['angle'] <= 360. or -90. < self.opts['angle'] <= -0.:
            p.translate(-self.boundingRect().topRight())
        pg.ArrowItem.paint(self, p, *args)


class MPCPlotter:
    def __init__(self, local_map_scale, global_map_scale,
                 local_map_size, global_map_size,
                 wall_avoidance_points,
                 direction_norm=0.7,
                 max_step=500):
        self.fw_loss = np.zeros(max_step)
        self.direction_norm = direction_norm
        self.local_map_size = local_map_size
        self.local_map_xmin = self.local_map_size[0][0]
        self.local_map_xmax = self.local_map_size[0][1]
        self.local_map_ymin = self.local_map_size[1][0]
        self.local_map_ymax = self.local_map_size[1][1]
        self.local_map_scale = local_map_scale

        self.global_map_size = global_map_size
        self.global_map_xmin = self.global_map_size[0][0]
        self.global_map_xmax = self.global_map_size[0][1]
        self.global_map_ymin = self.global_map_size[1][0]
        self.global_map_ymax = self.global_map_size[1][1]
        self.global_map_scale = global_map_scale

        pg.mkQApp()
        self.traces = {}

        # QtGui.QApplication.setGraphicsSystem('raster')

        # self.win = pg.GraphicsWindow(title="Plots")
        self.win = pg.GraphicsLayoutWidget()
        self.win.resize(1200,1500)
        self.win.setWindowTitle('Plots')
        qGraphicsGridLayout = self.win.ci.layout
        qGraphicsGridLayout.setColumnStretchFactor(0, 2)
        qGraphicsGridLayout.setColumnStretchFactor(1, 1)

        # Enable antialiasing for prettier plots
        pg.setConfigOptions(antialias=True)

        self.canvas = {}
        self.legend = {}

        self.canvas["local_map"] = self.win.addPlot(row=0, col=0, colspan=1, title="Local Costmap")
        # Item for displaying image data
        self.object_img = pg.ImageItem()
        self.social_img = pg.ImageItem()
        self.canvas["local_map"].addItem(self.object_img)
        self.canvas["local_map"].addItem(self.social_img)
        self.canvas["local_map"].addLegend(brush=pg.mkBrush(0, 0, 0, 150),
                                           offset=(-0, -0))

        self.robot_pos = self.canvas["local_map"].plot([0.], [0.],
                                                       pen=None,
                                                       symbol='o',
                                                       symbolPen=pg.mkPen(color=(0, 0, 0), width=0),
                                                       symbolBrush=pg.mkBrush(255, 105, 180, 255),
                                                       symbolSize=7,
                                                       name='Robot Pose')

        # self.plot_base = self.canvas["local_map"].plot(pen=pg.mkPen(color=(0, 255, 255)),
        #                                           name='Predicted Path')
        self.plot_base_orientation = self.canvas["local_map"].plot([0., 0.], [0., self.direction_norm], pen=pg.mkPen('r'))
        self.plot_pan_angle = self.canvas["local_map"].plot(pen=pg.mkPen('y'),
                                                            name='Pan Angle')

        self.plot_pan_goal = self.canvas["local_map"].plot(pen=None,
                                                           symbol='d',
                                                           symbolPen=pg.mkPen(color=(0, 0, 0), width=0),
                                                           symbolBrush=pg.mkBrush(255, 255, 0, 255),
                                                           symbolSize=7,
                                                           name='Pan Goal')

        self.plot_base_goal = self.canvas["local_map"].plot(pen=None,
                                                       symbol='d',
                                                       symbolPen=pg.mkPen(color=(0, 0, 0), width=0),
                                                       symbolBrush=pg.mkBrush(0, 255, 0, 255),
                                                       symbolSize=7,
                                                       name='Base Goal Pose')

        self.plot_base_goal_angle = self.canvas["local_map"].plot(pen=pg.mkPen('g'))

        self.plot_human_features = self.canvas["local_map"].plot(pen=None,
                                                                 symbol='d',
                                                                 symbolPen=pg.mkPen(color=(0, 0, 0), width=0),
                                                                 symbolBrush=pg.mkBrush(0, 255, 255, 255),
                                                                 symbolSize=7,
                                                                 name='Human Attention Pose')

        self.plot_human_features_angle = self.canvas["local_map"].plot(pen=pg.mkPen('c'))

        self.plot_base_wall_avoidance = self.canvas["local_map"].plot(wall_avoidance_points,
                                                                      pen=None,
                                                                      symbol='+',
                                                                      symbolPen=pg.mkPen(color=(204, 0, 204), width=0),
                                                                      symbolBrush=pg.mkBrush(204, 0, 204, 255),
                                                                      symbolSize=7,
                                                                      name='Wall avoidance Pose')

        self.canvas["loss"] = self.win.addPlot(row=0, col=1, colspan=1, title="Forward Loss ")
        self.canvas["loss"].addLegend()
        # self.plot_base_loss = self.canvas["loss"].plot(pen='y', name='Base Loss')
        # self.plot_visual_loss = self.canvas["loss"].plot(pen='c', name='Visual Loss')
        self.plot_fw_loss = self.canvas["loss"].plot(pen='c', name='Forward Loss')
        self.count = 0

        object_lut = (np.asarray(OBJECT_COLORMAP)*255).view(np.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt
        self.object_img.setLookupTable(object_lut)
        social_lut = (np.asarray(SOCIAL_COLORMAP)*255).view(np.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt
        self.social_img.setLookupTable(social_lut)

        self.canvas['global_map'] = self.win.addPlot(row=1, col=0, colspan=2, title="Global Map")
        # Item for displaying image data
        self.global_map = pg.ImageItem(border='w')
        self.canvas['global_map'].addItem(self.global_map)
        self.canvas['global_map'].addLegend()
        self.waypoint_arrow = []
        self.robot_pose_arrow = []
        self.target_arrow = None
        self.legend['global_map'] = pg.LegendItem(offset=(70, 50))
        self.legend['global_map'].setParentItem(self.canvas['global_map'])
        self.tangent_traj_vec_arrow = []
        self.plot_tangent_traj_vec = self.canvas["global_map"].plot(pen=None,
                                                                    symbol='+',
                                                                    symbolPen=pg.mkPen(color=(0, 255, 255), width=0),
                                                                    symbolBrush=pg.mkBrush(0, 255, 255, 255),
                                                                    symbolSize=7)

    def update(self, mpc_fw_loss, joint_angles, tangent_traj_vec=None, target=None, waypoint=None, robot_pose=None, global_map=None, local_map=None, base_goal=None, pan_goal=None, human_features=None):
        if local_map is not None:
            object_map = local_map[:, :, 0]
            social_map = local_map[:, :, 1]
            data = np.fliplr(np.transpose(object_map))
            self.object_img.setImage(np.array(data))
            self.object_img.setScale(1/self.local_map_scale)
            self.object_img.setPos(self.local_map_xmin, self.local_map_ymin)

            data = np.fliplr(np.transpose(social_map))
            self.social_img.setImage(np.array(data))
            self.social_img.setScale(1/self.local_map_scale)
            self.social_img.setPos(self.local_map_xmin, self.local_map_ymin)

        if global_map is not None:
            data = np.fliplr(np.transpose(global_map))
            self.global_map.setImage(data)
            self.global_map.setScale(1/self.global_map_scale)
            self.global_map.setPos(self.global_map_xmin, self.global_map_ymin)

        # positions = np.vstack([[0., 0.], []])  # mpc.fw_base_positions_pos_c])
        # self.plot_base.setData(positions[:,0], positions[:,1])
        self.clear_plot()

        if human_features[-1] > 0.:
            human_ang = utils.robot_frame_to_ssn_frame_ang(human_features[2])
            self.plot_human_features.setData([human_features[0]], [human_features[1]])
            self.plot_human_features_angle.setData([human_features[0], human_features[0] + self.direction_norm*np.cos(human_ang)], [human_features[1], human_features[1] + self.direction_norm*np.sin(human_ang)])

        if base_goal[-1] > 0.:
            base_goal_ang = utils.robot_frame_to_ssn_frame_ang(base_goal[2])
            self.plot_base_goal.setData([base_goal[0]], [base_goal[1]])
            self.plot_base_goal_angle.setData([base_goal[0], base_goal[0] + self.direction_norm*np.cos(base_goal_ang)], [base_goal[1], base_goal[1] + self.direction_norm*np.sin(base_goal_ang)])

        if pan_goal[-1] > 0.:
            joint_ang = utils.robot_frame_to_ssn_frame_ang(joint_angles[0])
            self.plot_pan_goal.setData([pan_goal[0]], [pan_goal[1]])
            self.plot_pan_angle.setData([0., self.direction_norm*np.cos(joint_ang)], [0., self.direction_norm*np.sin(joint_ang)])

        if self.count > 0:
            self.fw_loss[self.count % len(self.fw_loss)] = mpc_fw_loss
        self.plot_fw_loss.setData(np.hstack((self.fw_loss[self.count % len(self.fw_loss):], self.fw_loss[:self.count % len(self.fw_loss)])))

        if target is not None:
            if self.target_arrow:
                self.canvas['global_map'].removeItem(self.target_arrow)
                self.legend['global_map'].removeItem(self.target_arrow)
            ang = utils.box2d_frame_to_arrow_frame_ang(target[2])
            self.target_arrow = MyArrowItem(angle=np.degrees(ang), headWidth=15, headLen=10, tailLen=20, tailWidth=1, baseAngle=25, pen={'color': 'g'}, brush='g')
            self.target_arrow.setPos(target[0], target[1])
            self.canvas['global_map'].addItem(self.target_arrow)
            self.legend['global_map'].addItem(self.target_arrow, name='Final target pose')

        if waypoint is not None and self.count % 10 == 0.:
            ang = utils.box2d_frame_to_arrow_frame_ang(waypoint[2])
            self.waypoint_arrow.append(MyArrowItem(angle=np.degrees(ang), headWidth=15, headLen=10, tailLen=20, tailWidth=1, baseAngle=25, pen={'color': 'r'}, brush='r'))
            self.waypoint_arrow[-1].setPos(waypoint[0], waypoint[1])
            self.canvas['global_map'].addItem(self.waypoint_arrow[-1])
            if len(self.waypoint_arrow) == 1 :
                self.legend['global_map'].addItem(self.waypoint_arrow[-1], name='Intermediate waypoint')

        if robot_pose is not None and self.count % 10 == 0.:
            ang = utils.box2d_frame_to_arrow_frame_ang(robot_pose[2])
            self.robot_pose_arrow.append(MyArrowItem(angle=np.degrees(ang), headWidth=15, headLen=10, tailLen=20, tailWidth=1, baseAngle=25, pen={'color': 'b'}, brush='b'))
            self.robot_pose_arrow[-1].setPos(robot_pose[0], robot_pose[1])
            self.canvas['global_map'].addItem(self.robot_pose_arrow[-1])
            if len(self.robot_pose_arrow) == 1 :
                self.legend['global_map'].addItem(self.robot_pose_arrow[-1], name='Robot pose')

        if tangent_traj_vec is not None:
            ang = utils.box2d_frame_to_arrow_frame_ang(tangent_traj_vec[2])
            if self.tangent_traj_vec_arrow:
                    [self.canvas['global_map'].removeItem(item) for item in self.tangent_traj_vec_arrow]
                    [self.legend['global_map'].removeItem(item) for item in self.tangent_traj_vec_arrow]
                    self.tangent_traj_vec_arrow.clear()
            self.tangent_traj_vec_arrow.append(MyArrowItem(angle=np.degrees(ang), headWidth=15, headLen=10, tailLen=40, tailWidth=1, baseAngle=25, pen=pg.mkPen(color=(0, 255, 255, 100), width=0), brush=pg.mkBrush(0, 255, 255, 100)))
            self.tangent_traj_vec_arrow[0].setPos(tangent_traj_vec[0], tangent_traj_vec[1])
            self.legend['global_map'].addItem(self.tangent_traj_vec_arrow[0], name='Human pose')
            if robot_pose is not None and human_features[-1] > 0.:
                ang_h = utils.box2d_frame_to_arrow_frame_ang(human_features[2])
                self.tangent_traj_vec_arrow.append(MyArrowItem(angle=np.degrees(ang + ang_h - np.pi/2), headWidth=15, headLen=10, tailLen=40, tailWidth=1, baseAngle=25, pen=pg.mkPen(color=(0, 255, 255, 255), width=0), brush=pg.mkBrush(0, 255, 255, 255)))
                pos = utils.local_to_global(robot_pose, human_features[:2])
                self.tangent_traj_vec_arrow[1].setPos(pos[0], pos[1])
                self.legend['global_map'].addItem(self.tangent_traj_vec_arrow[1], name='Human target')
                self.plot_tangent_traj_vec.setData([tangent_traj_vec[0], pos[0]], [tangent_traj_vec[1], pos[1]])
            else:
                self.plot_tangent_traj_vec.setData([tangent_traj_vec[0]], [tangent_traj_vec[1]])
            [self.canvas['global_map'].addItem(item) for item in self.tangent_traj_vec_arrow]

        self.canvas["loss"].autoRange()
        # self.canvas["local_map"].autoRange()
        # self.canvas["global_map"].autoRange()
        self.canvas["local_map"].setXRange(self.local_map_xmin, self.local_map_xmax)
        self.canvas["local_map"].setYRange(self.local_map_ymin, self.local_map_ymax)
        self.canvas["global_map"].setXRange(self.global_map_xmin, self.global_map_xmax)
        self.canvas["global_map"].setYRange(self.global_map_ymin, self.global_map_ymax)

        self.count += 1
        self.win.show()
        QtGui.QApplication.processEvents()

    def clear_plot(self):
        self.plot_human_features.clear()
        self.plot_human_features_angle.clear()
        self.plot_base_goal.clear()
        self.plot_base_goal_angle.clear()
        self.plot_pan_goal.clear()
        self.plot_pan_angle.clear()
        self.plot_tangent_traj_vec.clear()

    def close(self):
        self.win.close()
