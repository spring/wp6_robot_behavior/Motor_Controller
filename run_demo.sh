#!/bin/bash

for i in "$@"
do
case $i in
    -d|--debug)
    DEBUG=True
    ;;
    -r|--recording)
    RECORDING=True
    ;;
    -g|--gui)
    GUI=False
    # by default true
    ;;
    -p|--plot)
    PLOT=False
    # true by default
    ;;
    -c=*|--config-name=*)
    CONFIG_NAME="${i#*=}"
    ;;
    -n=*|--nb_sim=*)
    NB_SIM="${i#*=}"
    ;;
    --mpc-branch=*)
    MPC_BRANCH="${i#*=}"
    ;;
    --gym-box2d-branch=*)
    GYM_BOX2D_BRANCH="${i#*=}"
    ;;
    -v|--verbose)
    VERBOSE=True
    ;;
    -s|--save)
    SAVE=True
    ;;
    *)
    echo "Argument error"
    exit 3
    ;;
esac
done

if [ -z $CONFIG_NAME ]
then
  CONFIG_NAME=basic_config
else
  CONFIG_NAME=$CONFIG_NAME
fi

if [ -z $MPC_BRANCH ]
then
  MPC_BRANCH=devel
else
  MPC_BRANCH=$MPC_BRANCH
fi

if [ -z $GYM_BOX2D_BRANCH ]
then
  GYM_BOX2D_BRANCH=devel
fi

if [ -z $VERBOSE ]
then
  VERBOSE=False
fi

if [ -z $SAVE ]
then
  SAVE=False
fi

if [ -z $GUI ]
then
  GUI=True
fi

if [ -z $RECORDING ]
then
  RECORDING=False
fi

if [ -z $PLOT ]
then
  PLOT=True
fi

if [ -z $NB_SIM ]
then
  NB_SIM=1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
python_file=$DIR/bash_args.py

echo CONFIG_NAME=$CONFIG_NAME
echo MPC_BRANCH=$MPC_BRANCH
echo GYM_BOX2D_BRANCH=$GYM_BOX2D_BRANCH
echo SAVE=$SAVE
echo VERBOSE=$VERBOSE
echo GUI=$GUI
echo RECORDING=$RECORDING
echo PLOT=$PLOT
echo NB_SIM = ${NB_SIM}

module1=sim2d
module2=social_mpc
cat >get_module_path.py << EOI
import $module1
import os
print(os.path.dirname(os.path.abspath($module1.__file__)))
EOI
module1_path=$(python get_module_path.py)
rm get_module_path.py
cat >get_module_path.py << EOI
import $module2
import os
print(os.path.dirname(os.path.abspath($module2.__file__)))
EOI
module2_path=$(python get_module_path.py)
rm get_module_path.py

git -C $module2_path checkout $MPC_BRANCH
if [ $VERBOSE = True ]
then
  git -C $module2_path status
fi
if [ $VERBOSE = True ]
then
  git -C $module1_path status
fi
git -C $module1_path checkout $GYM_BOX2D_BRANCH

cat >$python_file << END_SCRIPT
#!/usr/bin/env python
# -*- coding: utf-8 -*-

CONFIG_NAME = '$CONFIG_NAME'
MPC_BRANCH = '$MPC_BRANCH'
GYM_BOX2D_BRANCH = '$GYM_BOX2D_BRANCH'
SAVE = $SAVE
VERBOSE = $VERBOSE
GUI = $GUI
RECORDING=$RECORDING
PLOT = $PLOT
END_SCRIPT

# modifying settings to run SimpleScreenRecorder
if [ $RECORDING = True ]
then
  CONFIG_PATH=$DIR/social_mpc/config/settings_ssr.conf
  DATE=$(date +"%d_%m_%Y_%H_%M")
  OUTPUT_PATH=$DIR/results/data/videos/$DATE.mp4
  sed -i "s|\(^file= *\).*|\1$OUTPUT_PATH|" $CONFIG_PATH
  if [ $CONFIG_NAME == basic_config ]
  then
  LENGTH_RECORD=$(grep 'end_time:' $DIR/social_mpc/config/run_demo.yaml  | cut -c 11-)
  else
  LENGTH_RECORD=$(grep 'end_time:' $DIR/results/data/config/social_mpc/${CONFIG_NAME}_run_demo.yaml  | cut -c 11-)
  fi
  INIT_WAIT=10
  END_WAIT=5
fi

# echo $DIR, $module1_path, $module2_path, $CONFIG_PATH, $DATE, $LENGTH_RECORD
for i in $(seq 1 1 $((${NB_SIM}-1)))
do
  if [ $VERBOSE = True ]
  then
    if [ $RECORDING = True ]
    then
      # python $DIR/demo.py & simplescreenrecorder --settingsfile=$CONFIG_PATH --start-recording & (sleep 20 && record-pause && quit) # --start-hidden
      python $DIR/demo.py & (sleep $INIT_WAIT; echo record-start; sleep $LENGTH_RECORD; echo record-save; sleep 1;echo quit;sleep $END_WAIT) | (simplescreenrecorder --start-hidden --settingsfile=$CONFIG_PATH) &
    else
      python $DIR/demo.py &
    fi
  else
    if [ $RECORDING = True ]
    then
      python $DIR/demo.py 2>&1 >/dev/null & (sleep $INIT_WAIT; echo record-start; sleep $LENGTH_RECORD; echo record-save; sleep 1;echo quit;sleep $END_WAIT) | (simplescreenrecorder --start-hidden --settingsfile=$CONFIG_PATH) &
    else
      python $DIR/demo.py 2>&1 >/dev/null &
    fi
  fi
  sleep 3s
done
sleep 3s
if [ $VERBOSE = True ]
then
  if [ $RECORDING = True ]
  then
    # python $DIR/demo.py & simplescreenrecorder --settingsfile=$CONFIG_PATH --start-recording & (sleep 20 && record-pause && quit) # --start-hidden
    python $DIR/demo.py & (sleep $INIT_WAIT; echo record-start; sleep $LENGTH_RECORD; echo record-save; sleep 1;echo quit;sleep $END_WAIT) | (simplescreenrecorder --start-hidden --settingsfile=$CONFIG_PATH)
  else
    python $DIR/demo.py
  fi
else
  if [ $RECORDING = True ]
  then
    python $DIR/demo.py 2>&1 >/dev/null & (sleep $INIT_WAIT; echo record-start; sleep $LENGTH_RECORD; echo record-save; sleep 1;echo quit;sleep $END_WAIT) | (simplescreenrecorder --start-hidden --settingsfile=$CONFIG_PATH)
  else
    python $DIR/demo.py 2>&1 >/dev/null
  fi
fi
