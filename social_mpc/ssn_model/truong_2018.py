# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Chris Reinke, Alex Auternaud, Timothée Wintz
# chris.reinke@inria.fr
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import numpy as np
import social_mpc.ssn_model.group_detection as gd
from social_mpc.ssn_model.group_detection import Group
import matplotlib.pyplot as plt
import matplotlib.patches
from scipy import optimize
from scipy import ndimage
import scipy.interpolate
import skfmm
import time
from sim2d import utils
import cv2




class SocialSpaceNav:
    def __init__(self, world_size, config, objects=None):
        self.world_size = world_size
        self.objects = objects
        self.step_per_meter = 100/config.step_meshes  # cm sampling
        self.stride = config.group_detection_stride
        self.x_mesh, self.y_mesh = None, None

        self.default_hand_distance = 0.2

        self.A_p = 1.0
        self.sigma_p_x = 0.45
        self.sigma_p_y = 0.45
        self.alpha = np.pi / 2
        self.fov_angle = 60 * np.pi / 180
        self.f_v = 0.8
        self.f_front = 0.2
        self.f_fov = 0.
        self.F_sit = 2.5

        self.f_h = 0.0
        self.sigma_h_x = 0.28
        self.sigma_h_y = 0.28

        self.w_1 = 1.0
        self.w_2 = 1.0

        self.T_dis = 3.6
        self.T_ang = 10 * np.pi / 180

        self.f_g = 0.0

        self.w_3 = 1.0
        self.w_4 = 1.0

        self.r_step = 0.1
        self.r_p_0 = 0.1
        self.dsz_border = 0.03

        self.max_steps = 100
        self.position_step_width = 0.1  # in meter
        self.rotation_step_width = np.pi*0.1  # in rad
        self.min_goal_position_dist = 0.05  # in meter
        self.min_goal_rotation_dist = 0.05  # in rad

        self.sigma = 0.15

        self.alpha_sis = 0.95
        self.state_mem = None
        self.delta_pose = np.zeros(3)

        self.x_mesh, self.y_mesh = self.get_meshes()

        self.global_map = None

    def world_occupancy_map(self):
        """
        Create an empty velocity map with only the non dynamic objects (walls,
        desks, benches, etc.)
        """
        occupancy_map = self.get_default_occupany_map()
        if np.any(self.objects):
            for object in self.objects:
                occupancy_map = self.add_box_to_occupancy_map(occupancy_map, object) # (x, y, width, height)
        return occupancy_map


    def calc_group_circle(self, persons):
        persons_coordinates = persons[:, :2]
        x_g, y_g, r_g, _ = self.least_squares_circle(persons_coordinates)
        return x_g, y_g, r_g


    def least_squares_circle(self, coords):
        """
        Circle fit using least-squares solver.
        Inputs:

            - coords, list or numpy array with len>2 of the form:
            [
        [x_coord, y_coord],
        ...,
        [x_coord, y_coord]
        ]

        Outputs:

            - xc : x-coordinate of solution center (float)
            - yc : y-coordinate of solution center (float)
            - R : Radius of solution (float)
            - residu : MSE of solution against training data (float)

        Taken from package circle-fit.
        """

        # coordinates of the barycenter
        x = coords[:, 0]
        y = coords[:, 1]
        x_m = np.mean(x)
        y_m = np.mean(y)
        center_estimate = x_m, y_m
        center, _ = optimize.leastsq(self.f, center_estimate, args=(x,y))
        # r = optimize.least_squares(self.f, center_estimate, args=(x,y))
        # center = r.x
        xc, yc = center
        Ri = self.calc_R(x, y, *center)
        R = Ri.mean()
        residu = np.sum((Ri - R)**2)
        return xc, yc, R, residu


    def calc_R(self, x, y, xc, yc):
        """
        calculate the distance of each 2D points from the center (xc, yc)
        """
        return np.sqrt((x - xc) ** 2 + (y - yc) ** 2)


    def f(self, c, x, y):
        """
        calculate the algebraic distance between the data points
        and the mean circle centered at c=(xc, yc)
        """
        Ri = self.calc_R(x, y, *c)
        return Ri - Ri.mean()


    def calc_default_hand_positions(self, person):
        '''Calculates the default postion for the hands of a person, so that they are on the side of og'''

        x_p, y_p, theta_p, _, _ = person

        # define position as if person has a 0 angle orientation at position [0,0]
        pos_lh = np.transpose(np.array([[0,  self.default_hand_distance]]))
        pos_rh = np.transpose(np.array([[0, -self.default_hand_distance]]))

        # rotate the positions according
        rotation_mat = np.array([[np.cos(theta_p), -np.sin(theta_p)],
                                 [np.sin(theta_p),  np.cos(theta_p)]])

        pos_lh = np.transpose(np.matmul(rotation_mat, pos_lh))[0]
        pos_rh = np.transpose(np.matmul(rotation_mat, pos_rh))[0]

        # translate according to position of person
        x_lh = pos_lh[0] + x_p
        y_lh = pos_lh[1] + y_p
        x_rh = pos_rh[0] + x_p
        y_rh = pos_rh[1] + y_p

        return x_lh, y_lh, x_rh, y_rh


    def calc_single_eps(self, x, y, person, d_po=None):
        # if person.shape[0] == 5:
        x_p, y_p, theta_p, v_p, is_sitting = person
            # x_lh, y_lh, x_rh, y_rh = calc_default_hand_positions(person)
        # else:
            # x_p, y_p, theta_p, v_p, is_sitting, x_lh, y_lh, x_rh, y_rh = person

        f_sit = self.F_sit if is_sitting else 1.0

        sigma_p_y_i = self.sigma_p_y

        # personal space f_p
        d = np.sqrt((x - x_p) ** 2 + (y - y_p) ** 2)

        # Algorithm 1
        sigma_p_x_i = np.full_like(x, self.sigma_p_x)

        # angle according to the perspective of the person
        theta = np.arctan2(y - y_p, x - x_p)
        theta_diff = np.abs(np.arctan2(np.sin(theta - theta_p), np.cos(theta - theta_p)))

        # check if an object needs to be considered
        if not d_po:
            in_field_of_view_of_p_inds = theta_diff <= self.fov_angle
            in_frontal_area_of_p_inds = (theta_diff <= self.alpha) & ~in_field_of_view_of_p_inds

            sigma_p_x_i[in_field_of_view_of_p_inds] = (1 + v_p * self.f_v + self.f_front * f_sit + self.f_fov) * self.sigma_p_x
            sigma_p_x_i[in_frontal_area_of_p_inds] = (1 + v_p * self.f_v + self.f_front  * f_sit) * self.sigma_p_x
        else:
            in_frontal_area_of_p_inds = (theta_diff <= self.alpha)

            sigma_p_x_i[in_frontal_area_of_p_inds] = d_po / 2.0

        f_p = self.A_p * np.exp(-(d * np.cos(theta_diff) / (np.sqrt(2) * sigma_p_x_i)) ** 2 -
                                 (d * np.sin(theta_diff) / (np.sqrt(2) * sigma_p_y_i)) ** 2)

        # left hand space
        # d_lh = np.sqrt((x_p - x_lh) ** 2 + (y_p - y_lh) ** 2)
        # theta_lh = np.arctan2(y_lh - y_p, x_lh - x_p)
        # sigma_lh_x = (1 + self.f_h * d_lh) * self.sigma_h_x
        #
        # f_lh = A_p * np.exp(-(d * np.cos(theta - theta_lh) / (np.sqrt(2) * sigma_lh_x)) ** 2 -
        #                     (d * np.sin(theta - theta_lh) / (np.sqrt(2) * self.sigma_h_y)) ** 2)

        # right hand space
        # d_rh = np.sqrt((x_p - x_rh) ** 2 + (y_p - y_rh) ** 2)
        # theta_rh = np.arctan2(y_rh - y_p, x_rh - x_p)
        # sigma_rh_x = (1 + self.f_h * d_rh) * self.sigma_h_x
        #
        # f_rh = A_p * np.exp(-(d * np.cos(theta - theta_rh) / (np.sqrt(2) * sigma_rh_x)) ** 2 -
        #                     (d * np.sin(theta - theta_rh) / (np.sqrt(2) * self.sigma_h_y)) ** 2)

        # combine hand and person spaces
        # f_hands = np.maximum(f_lh, f_rh)
        # f_cur_eps = np.maximum(self.w_1 * f_p, self.w_2 * f_hands)

        return self.w_1 * f_p  # f_cur_eps


    def calc_eps(self, x, y, persons, objects=None):
        '''Calculates the extended personal space of a human.'''
        f_eps = np.zeros_like(x)

        for person in persons:
            x_p, y_p, theta_p, v_p, is_sitting = person[0:5]

            is_incorporated_object = False

            if objects is not None:
                for object in objects:

                    # check if the object should be considered
                    x_ob, y_ob = object
                    # check if object is in interaction with the person
                    d_po = np.sqrt((x_ob - x_p) ** 2 + (y_ob - y_p) ** 2)
                    theta_po = np.arctan2((y_ob - y_p), (x_ob - x_p))
                    theta_diff = np.abs(np.arctan2(np.sin(theta_po - theta_p), np.cos(theta_po - theta_p)))

                    # check if the object should be incorporated into eps
                    if d_po < self.T_dis and theta_diff < self.T_ang:
                        cur_f_eps = self.calc_single_eps(x, y, person, d_po=d_po)
                        f_eps = np.maximum(f_eps, cur_f_eps)

                        is_incorporated_object = True

            # if no object was incorporated into the eps, then calculate one without an object
            if not is_incorporated_object:
                cur_f_eps = self.calc_single_eps(x, y, person)
                f_eps = np.maximum(f_eps, cur_f_eps)

        return f_eps


    def _body_fun_calc_eps_step(self, carry, object):
        person, is_incorporated_object, x, y, f_eps = carry
        x_p, y_p, theta_p, _, _ = person
        # check if the object should be considered
        x_ob, y_ob = object
        # check if object is in interaction with the person
        d_po = np.sqrt((x_ob - x_p) ** 2 + (y_ob - y_p) ** 2)
        theta_po = np.arctan2((y_ob - y_p), (x_ob - x_p))
        theta_diff = np.abs(np.arctan2(np.sin(theta_po - theta_p), np.cos(theta_po - theta_p)))

        # check if the object should be incorporated into eps
        if d_po < self.T_dis and theta_diff < self.T_ang:
            cur_f_eps = self.calc_single_eps(x, y, person, d_po=d_po)
            f_eps = np.maximum(f_eps, cur_f_eps)

            is_incorporated_object = True
        return (person, is_incorporated_object, x, y, f_eps), f_eps


    def calc_sis(self, x, y, persons, theta_g=0.0, v_g= 0.0, delta_pose=[0., 0., 0.]):
        if persons.shape[0] == 1:
            f_group = np.zeros_like(x)
        else:
            # use a simple circle approximation if the center and radius of the group are not given
            x_g, y_g, r_g = self.calc_group_circle(persons)
            state = np.asarray([x_g, y_g, r_g])
            #if self.state_mem is None:
            self.state_mem = state
            """else:
                self.state_mem[0] = (self.state_mem[1] - delta_pose[1])*np.sin(delta_pose[-1]) + np.cos(delta_pose[-1])*(self.state_mem[0] - delta_pose[0])
                self.state_mem[1] = np.cos(delta_pose[-1])*(self.state_mem[1] - delta_pose[1]) - (self.state_mem[0] - delta_pose[0])*np.sin(delta_pose[-1])
                self.state_mem = self.alpha_sis*self.state_mem + (1 - self.alpha_sis)*state"""
            x_g, y_g, r_g  = self.state_mem
            return self.calc_sis_step(x, y, theta_g, v_g, x_g, y_g, r_g)


    def calc_sis_step(self, x, y, theta_g, v_g, x_g, y_g, r_g):
        sigma_g_x = (r_g / 2) * (1 + self.f_g)
        sigma_g_y = (r_g / 2) * (1 + self.f_g)

        theta = np.arctan2(y - y_g, x - x_g)
        # if the group moves then differentiate between area in movement direction and area oposite to the mvoement direction
        if v_g > 0.0:
            sigma_g_x = np.full_like(x, sigma_g_x)
            theta_diff = np.abs(np.arctan2(np.sin(theta - theta_g), np.cos(theta - theta_g)))
            sigma_g_x[theta_diff < np.pi/2] = (r_g / 2) * (1 + self.f_g + self.f_v * v_g)

        d = np.sqrt((x - x_g) ** 2 + (y - y_g) ** 2)

        f_group = self.A_p * np.exp(-(d * np.cos(theta - theta_g) / (np.sqrt(2) * sigma_g_x)) ** 2 -
                                     (d * np.sin(theta - theta_g) / (np.sqrt(2) * sigma_g_y)) ** 2)

        return f_group


    def calc_dsz(self, persons, delta_pose=[0., 0., 0.], groups=None, objects=None):
        # calc eps
        f_eps = self.calc_eps(self.x_mesh, self.y_mesh, persons, objects)

        # calc sis for all groups
        f_sis = np.zeros_like(self.x_mesh)
        if groups:
            for group in groups:

                # get persons of the group and the direction and velocity of the group
                theta_g, v_g, person_idxs = group[0], group[1], np.array(group[2:], dtype=np.int8)
                cur_persons = persons[person_idxs, :]

                cur_f_sis = self.calc_sis(self.x_mesh, self.y_mesh, cur_persons, theta_g, v_g, delta_pose=delta_pose)
                f_sis = np.maximum(f_sis, cur_f_sis)

        # combine both
        f_dsz = np.maximum(self.w_3 * f_eps, self.w_4 * f_sis)

        return f_dsz

    # helper function to define the points on a circle
    def calc_circle_points(self, x_c, y_c, r_c):
        return (np.abs(np.hypot(x_c - self.x_mesh, y_c - self.y_mesh) - r_c) < 0.04)

    def position_to_px_coordinates(self, x, y):
        y = int((y - self.world_size[1][0]) * self.step_per_meter + 0.5)
        x = int((x - self.world_size[0][0]) * self.step_per_meter + 0.5)
        return x, y


    def calc_goal_pos(self, f_dsz, persons, robot=[.0, .0], prev_goal=None):
        if persons.shape[0] == 1:
            # single person
            x_c, y_c = persons[0, 0], persons[0, 1]
            r_c = self.r_p_0
        else:
            # group of people
            x_c, y_c, r_c = self.calc_group_circle(persons)

        A_out = np.full_like(self.x_mesh, False, dtype=bool)
        while 1:
            r_c += self.r_step
            A_in = self.calc_circle_points(x_c, y_c, r_c)

            if not np.any(A_in):
                break

            A_in_1 = A_in.copy()
            A_out_1 = np.full_like(self.x_mesh, False, dtype=bool)


            # check for circle points that are within all persons viewing point
            for person in persons:

                x_p, y_p, theta_p = person[0], person[1], person[2]

                # calculate the angle of each point on the circle in relation to the person
                theta = np.arctan2(self.y_mesh[A_in_1] - y_p, self.x_mesh[A_in_1] - x_p)
                # angle according to the perspective of the person
                theta_diff = np.abs(np.arctan2(np.sin(theta - theta_p), np.cos(theta - theta_p)))

                # check if circle points are in the viewing angle of the person
                A_out_1[A_in_1] = (theta_diff <= self.fov_angle)

                # if there is no point that is viewed stop
                if not np.any(A_out_1):
                    break
                else:
                    # only check further points that are viewed by all people
                    A_in_1 = A_out_1.copy()

            if np.any(A_out_1):
                # check if the points are outside the DMZ
                points_outside_DSZ_inds = f_dsz[A_out_1] <= self.dsz_border
                A_out[A_out_1] = points_outside_DSZ_inds
            else:
                # there are no points on the circle that can be viewed by all people
                # --> just take points into account that are inside the DSZ
                points_outside_DSZ_inds = f_dsz[A_in] <= self.dsz_border
                A_out[A_in] = points_outside_DSZ_inds

            x, y = self.position_to_px_coordinates(x_c, y_c)



            # remove points behind walls using connected components of circle
            if self.global_map is not None:
                A_out_2 = np.zeros_like(self.x_mesh, dtype=np.uint8)
                A_out_2 = cv2.circle(A_out_2, (x, y), int(np.ceil((r_c + 0.04) * self.step_per_meter)), 1, cv2.FILLED)
                global_map = cv2.resize(self.global_map, A_out_2.shape[::-1])
                A_out_2 = np.array(A_out_2 * (global_map == 0), dtype=np.uint8)
                ccs = cv2.connectedComponents(A_out_2)
                cc_idx = ccs[1][y, x]
                A_out[ccs[1] != cc_idx] = 0

            if np.any(A_out):
                break

        # *_, A_out = while_loop(self.cond_fun_while_calc_goal_pos, self.body_fun_while_calc_goal_pos, (x_c, y_c, r_c, persons, f_dsz, A_out))

        A_out, goal = self.filter_approaching_areas(A_out, robot, (x_c, y_c), prev_goal)

        return goal, A_out


    def cond_fun_while_while_calc_goal_pos(self, val):
        exit, persons, A_in_1, A_out_1, i = val
        return i < persons.shape[0]


    def body_fun_while_while_calc_goal_pos(self, val):
        exit, persons, A_in_1, A_out_1, i = val
        x_p, y_p, theta_p = persons[i][:3]
        # calculate the angle of each point on the circle in relation to the person
        # theta = np.arctan2(self.y_mesh[A_in_1] - y_p, self.x_mesh[A_in_1] - x_p)
        theta = np.arctan2(np.where(A_in_1, self.y_mesh - y_p, 0.), np.where(A_in_1, self.x_mesh -x_p, 0))
        # angle according to the perspective of the person
        theta_diff = np.abs(theta - theta_p)  #np.abs(np.arctan2(np.sin(theta - theta_p), np.cos(theta - theta_p)))
        # check if circle points are in the viewing angle of the person
        # A_out_1[A_in_1] = (theta_diff <= self.fov_angle)
        # A_out_1 = index_update(A_out_1, index[A_in_1], tt)
        A_out_1 = np.where(A_in_1, theta_diff <= self.fov_angle, False)

        # if there is no point that is viewed stop
        # exit = cond(np.any(A_out_1),
                            # lambda _: 1,
                            # lambda _: 1,
                            # operand=None)
        # if ~np.any(A_out_1):
            # exit = True
        # else:
            # only check further points that are viewed by all people
            # A_in_1 = A_out_1.copy()
        A_in_1 = np.empty_like(A_out_1)
        # A_in_1 = index_update(A_in_1, index[:], A_out_1)
        A_in_1 = np.where(A_in_1 != A_out_1, A_out_1, A_in_1)
        i += 1
        return exit, persons, A_in_1, A_out_1, i


    def cond_fun_while_calc_goal_pos(self, val):
        *_, A_out = val
        return ~np.any(A_out)


    def body_fun_while_calc_goal_pos(self, val):
        x_c, y_c, r_c, persons, f_dsz, A_out = val
        r_c += self.r_step
        A_in = self.calc_circle_points(x_c, y_c, r_c)

        # A_in_1 = A_in.copy()
        A_in_1 = np.empty_like(A_in)
        # A_in_1 = index_update(A_in_1, index[:], A_in)
        A_in_1 = np.where(A_in_1 != A_in, A_in, A_in_1)

        A_out_1 = np.full_like(self.x_mesh, False, dtype=bool)
        # check for circle points that are within all persons viewing point
        exit = 0
        i = 0

        *_, A_in_1, A_out_1, _ = while_loop(self.cond_fun_while_while_calc_goal_pos, self.body_fun_while_while_calc_goal_pos, (exit, persons, A_in_1, A_out_1, i))
        # while i < len(persons) and exit == 0:
        # for person in persons:
            # x_p, y_p, theta_p = persons[i][:3]
            #
            # # calculate the angle of each point on the circle in relation to the person
            # # theta = np.arctan2(self.y_mesh[A_in_1] - y_p, self.x_mesh[A_in_1] - x_p)
            # theta = np.arctan2(np.where(A_in_1, self.y_mesh - y_p, 0.), np.where(A_in_1, self.x_mesh -x_p, 0))
            # # angle according to the perspective of the person
            # theta_diff = np.abs(theta - theta_p)  #np.abs(np.arctan2(np.sin(theta - theta_p), np.cos(theta - theta_p)))
            # # check if circle points are in the viewing angle of the person
            # # A_out_1[A_in_1] = (theta_diff <= self.fov_angle)
            # # A_out_1 = index_update(A_out_1, index[A_in_1], tt)
            # A_out_1 = np.where(A_in_1, theta_diff <= self.fov_angle, False)
            #
            # # if there is no point that is viewed stop
            # # exit = cond(np.any(A_out_1),
            #                     # lambda _: 1,
            #                     # lambda _: 1,
            #                     # operand=None)
            # # if ~np.any(A_out_1):
            #     # exit = True
            # # else:
            #     # only check further points that are viewed by all people
            #     # A_in_1 = A_out_1.copy()
            # A_in_1 = np.empty_like(A_out_1)
            # A_in_1 = index_update(A_in_1, index[:], A_out_1)
            # i += 1

        A_out = cond(np.any(A_out_1),
                     lambda _: np.where(A_out_1, np.where(A_out_1, f_dsz <= self.dsz_border, 0), A_out),
                     lambda _: np.where(A_in, np.where(A_in, f_dsz <= self.dsz_border, 0), A_out),
                     operand=None)
        # if np.any(A_out_1):
        #     # check if the points are outside the DMZ
        #     points_outside_DSZ_inds = f_dsz[A_out_1] <= self.dsz_border
        #     # A_out[A_out_1] = points_outside_DSZ_inds
        #     A_out = index_update(A_out, index[A_out_1], points_outside_DSZ_inds)
        # else:
        #     # there are no points on the circle that can be viewed by all people
        #     # --> just take points into account that are inside the DSZ
        #     points_outside_DSZ_inds = f_dsz[A_in] <= self.dsz_border
        #     # A_out[A_in] = points_outside_DSZ_inds
        #     A_out = index_update(A_out, index[A_in], points_outside_DSZ_inds)
        return x_c, y_c, r_c, persons, f_dsz, np.array(A_out, dtype=bool)


    def filter_approaching_areas(self, A_out, robot, target, prev_goal=None):
        A_out = A_out.astype(np.float32)

        # if there is already a goal (for same target, minimize target to previous goal, not to robot)
        if prev_goal is None:
            x_r, y_r = robot
        else:
            x_r, y_r = prev_goal
        x_t, y_t = target

        

        # identify connected areas in A_out
        labeled_A_out, n_areas = ndimage.measurements.label(A_out, np.ones((3,3), dtype=np.int32))

        min_d_r = np.inf
        x_q = np.array([np.nan, np.nan])

        # ignore area 1
        for area_id in range(1, n_areas+1):

            label_inds = (labeled_A_out == area_id)

            # center point of area (true center of mass)
            center_x, center_y = ndimage.measurements.center_of_mass(label_inds)

            # identify the point in the 2d matrix that is the closest to the true center
            grid_x, grid_y = np.indices(A_out.shape).astype(np.float32)
            d = np.sqrt((grid_x - center_x) ** 2 + (grid_y - center_y) ** 2)

            # replace A_out with the distance
            A_out[label_inds] = d[label_inds]

            # find coordinates with the shortest distance
            # min_d_grid_idx = np.nonzero(np.atleast_1d(d == np.min(d)))
            min_d_grid_idx = np.where(d == np.min(d))
            x_a_idx = min_d_grid_idx[0][0]
            y_a_idx = min_d_grid_idx[1][0]

            x_a = self.x_mesh[x_a_idx, y_a_idx]
            y_a = self.y_mesh[x_a_idx, y_a_idx]

            if prev_goal is None:

                # distance to the robot
                d_r = np.sqrt((x_a - x_r) ** 2 + (y_a - y_r) ** 2)

                if d_r < min_d_r:
                    min_d_r = d_r
                    x_q[:] = x_a, y_a
            else:
                # distance to the robot
                d_r = np.sqrt((x_a - x_r) ** 2 + (y_a - y_r) ** 2)

                if d_r < min_d_r:
                    min_d_r = d_r
                    x_q[:] = x_a, y_a

        # calculate the goal orientation
        theta_q = np.arctan2(y_t - x_q[1], x_t - x_q[0])
        goal = np.array([x_q[0], x_q[1], theta_q])

        return A_out, goal


    def calc_path(self, velocity_map, robot, goal):
        x_goal, y_goal, theta_goal = goal
        x_robot, y_robot = robot[0], robot[1]

        # TODO: also update the rotation of the robot
        theta_robot = theta_goal

        # compute the shortest travel time map based on the velocity map
        # TODO: if the goal location is not reachable, then select the most nearby point that is reachable

        # define the goal location to the point in the world grid that is closest to the goal location
        # phi there is -1, otherwise it is 1
        distance_to_goal = np.sqrt((self.x_mesh - x_goal) ** 2 + (self.y_mesh - y_goal) ** 2)
        phi = np.ones_like(self.x_mesh)
        phi[np.unravel_index(np.argmin(distance_to_goal), phi.shape)] = -1

        travel_time_map = skfmm.travel_time(phi, velocity_map, order=1)

        # calc gradient iver travel_time map and create interpolation functions
        grad_t_y, grad_t_x = np.gradient(travel_time_map)

        # interpolate the gradients and velocities
        gradx_interp = scipy.interpolate.RectBivariateSpline(self.x_mesh[0, :], self.y_mesh[:, 0], grad_t_x)
        grady_interp = scipy.interpolate.RectBivariateSpline(self.x_mesh[0, :], self.y_mesh[:, 0], grad_t_y)
        velocity_interp = scipy.interpolate.RectBivariateSpline(self.x_mesh[0, :], self.y_mesh[:, 0], velocity_map)

        path = np.zeros((self.max_steps, 3))
        path[0, :] = [x_robot, y_robot, theta_robot]

        # create path by following the gradient of the travel time
        for step in range(self.max_steps):

            position_goal_distance = np.sqrt((x_robot - x_goal) ** 2 + (y_robot - y_goal) ** 2)
            rotation_goal_distance = np.abs(np.arctan2(np.sin(theta_robot - theta_goal), np.cos(theta_robot - theta_goal)))

            if position_goal_distance <= self.min_goal_position_dist and rotation_goal_distance <= self.min_goal_rotation_dist:
                break

            movement_direction = np.array([-gradx_interp(y_robot, x_robot)[0][0],
                                           -grady_interp(y_robot, x_robot)[0][0]])

            movement_speed = velocity_interp(y_robot, x_robot)[0][0]

            # create normalized movement direction vector and change its size according to the movement speed
            f_position = movement_direction / np.linalg.norm(movement_direction) * movement_speed

            # update the position of the robot
            x_robot += f_position[0] * self.position_step_width
            y_robot += f_position[1] * self.position_step_width

            f_orientation = 0.0
            # TODO: adapt the rotation of the robot
            path[step +1, :] = [x_robot, y_robot, theta_robot]

        path = path[:step+1, :]
        return path


    def occupancy_map_to_velocity_map(self, occupancy_map):
        '''Create a velocity map for a given occupancy map.'''

        d_x = [self.x_mesh[0, 1] - self.x_mesh[0, 0], self.y_mesh[1, 0] - self.y_mesh[0, 0]]

        # use fast marching method to compute the min distance from free to occupied areas
        phi = np.ones_like(occupancy_map, dtype=np.int8)
        phi[occupancy_map == 1] = -1

        distance_map = skfmm.distance(phi, d_x, order=1)

        # compute velocity based on distance
        velocity_map = 1.0 - np.exp(-distance_map/(np.sqrt(2)*self.sigma))
        velocity_map[occupancy_map == 1] = 0

        return velocity_map


    def get_meshes(self):
        # define the mesh of the environment
        x_coordinates = np.linspace(self.world_size[0][0], self.world_size[0][1], int((self.world_size[0][1]-self.world_size[0][0])*self.step_per_meter))
        y_coordinates = np.linspace(self.world_size[1][0], self.world_size[1][1], int((self.world_size[1][1]-self.world_size[1][0])*self.step_per_meter))
        x_mesh, y_mesh = np.meshgrid(x_coordinates, y_coordinates)
        return x_mesh, y_mesh


    def get_default_occupany_map(self):
        occupancy_map = np.full_like(self.x_mesh, False, dtype=bool)

        # borders
        occupancy_map[0, :] = True
        occupancy_map[-1, :] = True
        occupancy_map[:, 0] = True
        occupancy_map[:, -1] = True

        return occupancy_map


    def add_box_to_occupancy_map(self, occupancy_map, box):
        occupancy_map = np.copy(occupancy_map)

        x_box, y_box, width, height = box

        # identify the starting in x
        x_start = x_box
        x_end = x_start + width
        y_start = y_box
        y_end = y_box + height

        d_1_start_index = np.argmin(np.abs(self.x_mesh[0, :] - x_start))
        d_1_end_index = np.argmin(np.abs(self.x_mesh[0, :] - x_end))

        d_0_start_index = np.argmin(np.abs(self.y_mesh[:, 0] - y_start))
        d_0_end_index = np.argmin(np.abs(self.y_mesh[:, 0] - y_end))

        occupancy_map[d_0_start_index:d_0_end_index, d_1_start_index:d_1_end_index] = True

        return occupancy_map


    def plot_2d(self, space_model, occupancy_map=None, approaching_areas=None, persons=None, objects=None, goal=None, robot=[.0, .0], path=None, title='', is_contur_lines=False):
        origin = 'lower'

        fig, ax = plt.subplots(constrained_layout=True)

        CS = ax.contourf(self.x_mesh, self.y_mesh, space_model, 20, cmap=plt.cm.binary, origin=origin)

        # Make a colorbar for the ContourSet returned by the contourf call.
        cbar = fig.colorbar(CS)
        cbar.ax.set_ylabel('space model value')
        # Add the contour line levels to the colorbar
        if is_contur_lines:
            # Note that in the following, we explicitly pass in a subset of
            # the contour levels used for the filled contours.  Alternatively,
            # We could pass in additional levels to provide extra resolution,
            # or leave out the levels kwarg to use all of the original levels.
            CS2 = ax.contour(CS, levels=CS.levels[::2], colors='r', origin=origin)
            cbar.add_lines(CS2)

        if approaching_areas is not None:
            inds = approaching_areas > 0
            ax.scatter(self.x_mesh[inds], self.y_mesh[inds], s=1, c=-approaching_areas[inds])

        if robot is not None:
            ax.scatter(robot[0], robot[1], s=150, c='g')

        if goal is not None:
            # print goal position and orientation
            ax.scatter(goal[0], goal[1], c='r')
            plt.arrow(goal[0], goal[1], np.cos(goal[2]) * 0.1, np.sin(goal[2]) * 0.1, edgecolor='r')

        # draw persons
        if persons is not None:
            for person in persons:
                if len(person) == 5:
                    x_p, y_p, theta_p, v_p, is_sitting = person
                    # x_lh, y_lh, x_rh, y_rh = self.calc_default_hand_positions(persons[i, :])
                else:
                    x_p, y_p, theta_p, v_p, is_sitting, x_lh, y_lh, x_rh, y_rh = person

                # person
                e_p = matplotlib.patches.Ellipse((x_p, y_p), 0.15, 0.25,
                                                 angle=theta_p*180/np.pi, linewidth=2, fill=False, zorder=2, edgecolor='b')
                ax.add_patch(e_p)

                # left hand
                # e_lh = matplotlib.patches.Ellipse((x_lh, y_lh), 0.05, 0.05, linewidth=1, fill=False, zorder=2, edgecolor='b')
                # ax.add_patch(e_lh)

                # right hand
                # e_rh = matplotlib.patches.Ellipse((x_rh, y_rh), 0.05, 0.05, linewidth=1, fill=False, zorder=2, edgecolor='b')
                # ax.add_patch(e_rh)

                plt.arrow(x_p, y_p, np.cos(theta_p) * 0.1, np.sin(theta_p) * 0.1, edgecolor='b')

        # draw objects
        if objects is not None:
            for object in objects:
                x_ob, y_ob = object
                e = matplotlib.patches.Rectangle((x_ob-0.1, y_ob-0.1), 0.2, 0.2, edgecolor='r')
                ax.add_patch(e)

        if occupancy_map is not None:
            ax.scatter(self.x_mesh[occupancy_map == 1], self.y_mesh[occupancy_map == 1], c='k')

        if path is not None:
            path_position = np.array([[point[0], point[1]] for point in path])
            ax.plot(path_position[:,0], path_position[:,1])

        ax.set_title(title)
        ax.set_xlabel('x')
        ax.set_ylabel('y')

        plt.show()
