# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Chris Reinke, Alex Auternaud, Timothée Wintz
# chris.reinke@inria.fr
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import numpy as np
from scipy import interpolate


class Group:
    def __init__(self, center, person_ids):
        self.center = center
        self.person_ids = person_ids

class DynamicGroup(Group):
    def __init__(self, theta, v, group):
        super().__init__(group.center, group.person_ids)
        self.theta = theta
        self.v = v

class SocialSpacesConfig():
    def __init__(self,
            default_hand_distance,
            A_p,
            sigma_p_x,
            sigma_p_y,
            alpha,
            fov_angle,
            f_v,
            f_front,
            f_fov,
            F_sit,
            f_h,
            sigma_h_x,
            sigma_h_y,
            w_1,
            w_2,
            T_dis,
            T_ang,
            f_g,
            w_3,
            w_4,
            r_step,
            r_p_0,
            dsz_border,
            dsz_border_zero,
            max_steps,
            position_step_width,
            rotation_step_width,
            min_goal_position_dist,
            min_goal_rotation_dist,
            sigma,
            alpha_sis,
            state_mem,
            delta_pose):
        self.A_p = A_p
        self.sigma_p_x = sigma_p_x
        self.sigma_p_y = sigma_p_y
        self.alpha = alpha
        self.fov_angle = fov_angle
        self.f_v = f_v
        self.f_front = f_front
        self.f_fov = f_fov
        self.F_sit = F_sit
        self.f_h = f_h
        self.sigma_h_x = sigma_h_x
        self.sigma_h_y = sigma_h_y
        self.w_1 = w_1
        self.w_2 = w_2
        self.T_dis = T_dis
        self.T_ang = T_ang
        self.f_g = f_g
        self.w_3 = w_3
        self.w_4 = w_4
        self.r_step = r_step
        self.r_p_0 = r_p_0
        self.dsz_border = dsz_border
        self.dsz_border_zero = dsz_border_zero
        self.max_steps = max_steps
        self.position_step_width = position_step_width
        self.rotation_step_width = rotation_step_width
        self.min_goal_position_dist = min_goal_position_dist
        self.min_goal_rotation_dist = min_goal_rotation_dist
        self.sigma = sigma
        self.alpha_sis = alpha_sis
        self.state_mem = state_mem
        self.delta_pose = delta_pose

    @staticmethod
    def default_config():
        return SocialSpacesConfig(
            default_hand_distance = 0.2,
            A_p = 1.0,
            sigma_p_x = 0.2,  # 0.45,
            sigma_p_y = 0.2,  # 0.45,
            alpha = np.pi / 2,
            fov_angle = 60 * np.pi / 180,
            f_v = 0.8,
            f_front = 0.2,
            f_fov = 0.,
            F_sit = 2.5,
            f_h = 0.0,
            sigma_h_x = 0.28,
            sigma_h_y = 0.28,
            w_1 = 1.0,
            w_2 = 1.0,
            T_dis = 3.6,
            T_ang = 10 * np.pi / 180,
            f_g = -0.5,  # 0.0,
            w_3 = 1.0,
            w_4 = 1.0,
            r_step = 0.1,
            r_p_0 = 0.1,
            dsz_border = 0.001,
            dsz_border_zero = 0.001,
            max_steps = 100,
            position_step_width = 0.1,  # in meter,
            rotation_step_width = np.pi*0.1 , # in rad,
            min_goal_position_dist = 0.05,  # in meter,
            min_goal_rotation_dist = 0.05,  # in rad,
            sigma = 0.15,
            alpha_sis = 0.95,
            state_mem = None,
            delta_pose = np.zeros(3),
        )

class SocialSpaces():
    def __init__(self, map_shape, bbox, config=None):
        if config is None:
            config = SocialSpacesConfig.default_config()
        self.config = config
        self.map_shape = map_shape
        self.bbox = bbox

        self._init_grid()

    def _init_grid(self):
        self.y_coordinates = np.linspace(self.bbox[1][0], self.bbox[1][1], self.map_shape[0])
        self.x_coordinates = np.linspace(self.bbox[0][0], self.bbox[0][1], self.map_shape[1])
        self.x_grid, self.y_grid = np.meshgrid(self.x_coordinates, self.y_coordinates)

    def _pos_to_px(self, x, y):
        pass

    def _px_to_pos(self, i, j):
        pass

    def calc_circle_radius(self, center, persons):
        return np.sqrt((persons[:, 0] - center[0])**2 + (persons[:, 1] - center[1])**2).mean()

    def calc_single_eps(self, x, y, person, d_po=None):
        # if person.shape[0] == 5:
        x_p, y_p, theta_p, v_p, is_sitting = person

        f_sit = self.config.F_sit if is_sitting else 1.0

        sigma_p_y_i = self.config.sigma_p_y

        # personal space f_p
        d = np.sqrt((x - x_p) ** 2 + (y - y_p) ** 2)

        # Algorithm 1
        sigma_p_x_i = np.full_like(x, self.config.sigma_p_x)

        # angle according to the perspective of the person
        theta = np.arctan2(y - y_p, x - x_p)
        theta_diff = np.abs(np.arctan2(np.sin(theta - theta_p), np.cos(theta - theta_p)))

        # check if an object needs to be considered
        if not d_po:
            in_field_of_view_of_p_inds = theta_diff <= self.config.fov_angle
            in_frontal_area_of_p_inds = (theta_diff <= self.config.alpha) & ~in_field_of_view_of_p_inds

            sigma_p_x_i[in_field_of_view_of_p_inds] = (1 + v_p * self.config.f_v + self.config.f_front * f_sit + self.config.f_fov) * self.config.sigma_p_x
            sigma_p_x_i[in_frontal_area_of_p_inds] = (1 + v_p * self.config.f_v + self.config.f_front  * f_sit) * self.config.sigma_p_x
        else:
            in_frontal_area_of_p_inds = (theta_diff <= self.config.alpha)

            sigma_p_x_i[in_frontal_area_of_p_inds] = d_po / 2.0

        f_p = self.config.A_p * np.exp(-(d * np.cos(theta_diff) / (np.sqrt(2) * sigma_p_x_i)) ** 2 -
                                 (d * np.sin(theta_diff) / (np.sqrt(2) * sigma_p_y_i)) ** 2)


        return self.config.w_1 * f_p  # f_cur_eps

    def calc_eps(self, x, y, persons, objects=None):
        """Calculates the extended personal space of a human."""
        f_eps = np.zeros_like(x)

        for person in persons:
            x_p, y_p, theta_p, v_p, is_sitting = person[0:5]

            is_incorporated_object = False

            if objects is not None:
                for object in objects:

                    # check if the object should be considered
                    x_ob, y_ob = object
                    # check if object is in interaction with the person
                    d_po = np.sqrt((x_ob - x_p) ** 2 + (y_ob - y_p) ** 2)
                    theta_po = np.arctan2((y_ob - y_p), (x_ob - x_p))
                    theta_diff = np.abs(np.arctan2(np.sin(theta_po - theta_p), np.cos(theta_po - theta_p)))

                    # check if the object should be incorporated into eps
                    if d_po < self.config.T_dis and theta_diff < self.config.T_ang:
                        cur_f_eps = self.calc_single_eps(x, y, person, d_po=d_po)
                        f_eps = np.maximum(f_eps, cur_f_eps)

                        is_incorporated_object = True

            # if no object was incorporated into the eps, then calculate one without an object
            if not is_incorporated_object:
                cur_f_eps = self.calc_single_eps(x, y, person)
                f_eps = np.maximum(f_eps, cur_f_eps)

        return f_eps


    def calc_sis(self, x, y, persons, group_center, theta_g=0.0, v_g= 0.0):
        if persons.shape[0] == 1:
            f_group = np.zeros_like(x)
        else:
            x_g, y_g = group_center
            r_g = self.calc_circle_radius(group_center, persons)

            sigma_g_x = (r_g / 2) * (1 + self.config.f_g)
            sigma_g_y = (r_g / 2) * (1 + self.config.f_g)

            theta = np.arctan2(y - y_g, x - x_g)
            # if the group moves then differentiate between area in movement direction and area oposite to the mvoement direction
            if v_g > 0.0:
                sigma_g_x = np.full_like(x, sigma_g_x)
                theta_diff = np.abs(np.arctan2(np.sin(theta - theta_g), np.cos(theta - theta_g)))
                sigma_g_x[theta_diff < np.pi/2] = (r_g / 2) * (1 + self.config.f_g + self.config.f_v * v_g)

            d = np.sqrt((x - x_g) ** 2 + (y - y_g) ** 2)
            f_group = self.config.A_p * np.exp(-(d * np.cos(theta - theta_g) / (np.sqrt(2) * sigma_g_x)) ** 2 -
                                        (d * np.sin(theta - theta_g) / (np.sqrt(2) * sigma_g_y)) ** 2)
        return f_group


    def calc_dsz(self, persons, groups=None, objects=None):
        # calc eps
        f_eps = self.calc_eps(self.x_grid, self.y_grid, persons, objects)

        # calc sis for all groups
        f_sis = np.zeros_like(self.x_grid)
        if groups:
            for group in groups:
                # get persons of the group and the direction and velocity of the group
                if (isinstance(group, DynamicGroup)):
                    theta_g, v_g = group.theta, group.v
                else:
                    theta_g, v_g = 0.,0.
                cur_persons = persons[group.person_ids, :]
                group_center = group.center

                cur_f_sis = self.calc_sis(self.x_grid, self.y_grid, cur_persons, group_center, theta_g, v_g)
                f_sis = np.maximum(f_sis, cur_f_sis)


        # combine both
        f_dsz = np.maximum(self.config.w_3 * f_eps, self.config.w_4 * f_sis)

        return f_dsz

    def _circle_points(self, x_c, y_c, r, n):
        pts = np.zeros((n, 2))
        theta = np.linspace(0, 2*np.pi, n)
        pts[:, 0] = x_c + r * np.cos(theta)
        pts[:, 1] = y_c + r * np.sin(theta)
        return pts

    def calc_goal_pos(self, f_dsz, map, persons, group_center, robot=None):
        assert(map.shape == self.map_shape)
        r_max = 5
        n_points = 100
        r_step = 1.1
        if persons.shape[0] == 1:
            # single person
            r = self.config.r_p_0
        else:
            r = self.calc_circle_radius(group_center, persons)

        dsz_interp = interpolate.RectBivariateSpline(self.y_coordinates, self.x_coordinates, f_dsz)
        map_interp = interpolate.RectBivariateSpline(self.y_coordinates, self.x_coordinates, map)


        while True:
            circle_pts = self._circle_points(group_center[0], group_center[1], r, n_points)

            circle_map = map_interp.ev(circle_pts[:,1], circle_pts[:,0])
            circle_dsz = dsz_interp.ev(circle_pts[:,1], circle_pts[:,0])

            circle_dsz[circle_map > 0.5] = self.config.dsz_border + 1
            accept_idx = circle_dsz <= self.config.dsz_border
            circle_pts = circle_pts[accept_idx, :]

            if circle_pts.shape[0] > 0:
                break
            r *= r_step  
        if robot is not None:
            idx = np.argmin((circle_pts[:, 0] - robot[0])**2 + (circle_pts[:, 1] - robot[1])**2)
        else:
            circle_dsz = dsz_interp.ev(circle_pts[:,1], circle_pts[:,0])
            idx = np.argmin(circle_dsz)
        x, y = circle_pts[idx, 0], circle_pts[idx, 1]
        theta_q = np.arctan2(group_center[1] - y, group_center[0] - x)
        return x, y, theta_q