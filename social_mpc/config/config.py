# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Chris Reinke, Alex Auternaud, Timothée Wintz
# chris.reinke@inria.fr
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import numpy as np

class TemplatedConfig:
    template = {}
    def __init__(self, dict):
        for k in self.template.keys():
            if k not in dict:
                raise ValueError("Missing config key {0}".format(k))
            elif type(self.template[k]) == list:
                if dict[k] not in self.template[k]:
                    raise ValueError("Invalid config key {0} (must be one of {1})".format(k, str(self.template[k])))
            elif type(self.template[k]) == tuple and self.template[k][0] == np.array:
                try:
                    self.__setattr__(k, np.array(dict[k]))
                    assert(self.__getattribute__(k).shape == self.template[k][1])
                except:
                    raise ValueError("Invalid config key {0} (must be numpy array)".format(k))
            elif type(dict[k]) != self.template[k]:
                raise ValueError("Invalid config key {0} (must be {1})".format(k,  self.template[k]))
            else:
                self.__setattr__(k, dict[k])


class RunDemoConfig(TemplatedConfig):
    template = {
        "logging_mode": str,
        "max_steps": int,
        "end_time": int,
        "save_data": bool,
        "h_idx_to_save": int,
        "plot_render": bool,
    }


class ControllerConfig(TemplatedConfig):
    template = {
        "logging_mode": str,
        "h" : float,
        # "fw_horizon" : int,
        "fw_time": float,
        "u_lb": (np.array, (3,)),
        "u_ub": (np.array, (3,)),
        "reg_parameter": (np.array, (3,)),
        "max_acceleration": (np.array, (3,)),
        "max_iter_optim": int,
        "weights_description": list,
        "goto_target_dim": int,
        "human_target_dim": int,
        "pan_target_dim": int,
        "target_pose_goto_dim": int,
        "target_pose_look_dim": int,
        "goto_target_id": int,
        "goto_target_pose": (np.array, (5,)),
        "human_target_id": int,
        "pan_target_id": int,
        "min_dist_to_robot": float,
        "check_object_fw_traj_offset": float,
        "relative_h_pose": (np.array, (2,)),
        "loss": int,
        "goto_weight" : float,
        "human_features_weight": float,
        "object_cost_map_weight": float,
        "social_cost_map_weight": float,
        "wall_avoidance_weight": float,
        "cost_map_weight": float,
        "loss_rad": (np.array, (3,7)),
        "loss_coef": (np.array, (3,7)),
        "group_detection_stride": float,
        "step_meshes": int,
        "waypoint_distance": float,
        "orientation_distance_threshold":float,
        "wall_avoidance_points": (np.array,(1, 2)),
        "path_loop_time": float,
        "goal_loop_time": float,
        "goal_finder_enabled": bool,
        "path_planner_enabled": bool,
        "update_goals_enabled": bool,
        "dilation_iter": int
    }


class RobotConfig(TemplatedConfig):
    template = {
        "base_footprint2base_link" : (np.array, (3,)),
        "base_link2wheel_left_link" : (np.array, (3,)),
        "base_link2wheel_right_link" : (np.array, (3,)),
        "base_link2caster_left_link" : (np.array, (3,)),
        "base_link2caster_right_link" : (np.array, (3,)),
        "base_link2head_1_link" : (np.array, (3,)),
        "base_link2head_2_link" : (np.array, (3,)),
        "base_link2head_front_camera_link" : (np.array, (3,)),
        "base_link2head_front_camera_optical_frame" : (np.array, (3,)),
        "base_footprint_radius": float,
        "wheel_radius" : float,
        "wheel_width" : float,
        "wheel_separation" : float,
        "wheel_torque" : float,
        "wheel_velocity" : float,
        "caster_radius" : float,
        "caster_width" : float,
        "caster_separation_x" : float,
        "caster_offset_y" : float,
        "caster_offset_z" : float,
        "total_mass" : float,
        "wheel_mass" : float,
        "L1" : (np.array, (4,4)),
        "L2" : (np.array, (4,4)),
        "L3" : (np.array, (4,4)),
        "local_map_depth": float,
        "local_map_px_size":float,
        "max_speed_motor_wheel" : float,
        "max_forward_speed": float,
        "max_backward_speed": float,
        "max_forward_acc": float,
        "max_ang_vel": float,
        "max_ang_acc": float,
        "max_pan_angle" : float,
        "min_pan_angle" : float,
        "max_tilt_angle" : float,
        "min_tilt_angle" : float,
        "max_speed_motor_head" : float,
        "head_friction" : float,
        "head_damping" : float,
        "head_cam_fov_h" : float,
        "head_cam_fov_v" : float,
        "base_cam_fov_h" : float,
        "has_pan" : bool,
        "has_tilt" : bool
    }