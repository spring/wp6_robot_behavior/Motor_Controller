# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Alex Auternaud, Timothée Wintz
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import gym
import exputils as eu
import numpy as np
import pkg_resources
from social_mpc.controller import RobotController
from sim2d.gym_env import DiffDriveEnv
from social_mpc import utils
from social_mpc.plots import MPCPlotter


class MpcRobotSimEnv(gym.Env):

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            delta=1.0,
            sim_config_path=pkg_resources.resource_filename('social_mpc', 'config_rl/rl'),
            controller_config_filename=pkg_resources.resource_filename('social_mpc', 'config_rl/social_mpc.yaml'),
            n_mpc_steps=1,
            sim_gui=False,
        )
        return dc


    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        # init simulator and controller
        self.sim = DiffDriveEnv(gui=self.config.sim_gui, config_path=self.config.sim_config_path)
        self.controller = None

        # define spaces
        sim_x_min = self.sim.env_config.global_map_size[0][0]
        sim_x_max = self.sim.env_config.global_map_size[0][1]
        sim_y_min = self.sim.env_config.global_map_size[1][0]
        sim_y_max = self.sim.env_config.global_map_size[1][1]

        screen_height = self.sim.env_config.global_map_height
        screen_width = self.sim.env_config.global_map_width

        self.observation_space = gym.spaces.Dict(
            human_position_map=gym.spaces.Box(low=0, high=1, shape=(screen_height, screen_width), dtype=bool),

            human_orientation_map=gym.spaces.Box(low=0., high=np.pi, shape=(screen_height, screen_width), dtype=np.float32),

            human_linear_velocity_map=gym.spaces.Box(low=0., high=np.inf, shape=(screen_height, screen_width), dtype=np.float32),

            human_angular_velocity_map=gym.spaces.Box(low=-np.inf, high=np.inf, shape=(screen_height, screen_width), dtype=np.float32),

            occupancy_map=gym.spaces.Box(low=0, high=1, shape=(screen_height, screen_width), dtype=bool),

            robot_pose=gym.spaces.Box(low=np.array([sim_x_min, sim_y_min, -np.pi], dtype=np.float32),
                                      high=np.array([sim_x_max, sim_y_max, np.pi], dtype=np.float32)),

            joint_angles=gym.spaces.Box(low=np.array([self.sim.robot_config.min_pan_angle], dtype=np.float32),
                                        high=np.array([self.sim.robot_config.max_pan_angle], dtype=np.float32)),
        )

        self.action_space = gym.spaces.Dict(
            goto_goal=gym.spaces.Box(low=np.array([sim_x_min, sim_y_min, -np.pi], dtype=np.float32),
                                     high=np.array([sim_x_max, sim_y_max, np.pi], dtype=np.float32)),

            pan_goal=gym.spaces.Box(low=np.array([sim_x_min, sim_y_min], dtype=np.float32),
                                    high=np.array([sim_x_max, sim_y_max], dtype=np.float32)),

            weights=gym.spaces.Box(low=np.array([0., 0., 0.], dtype=np.float32),
                                   high=np.array([3., 3000., 600.], dtype=np.float32)),

            reg_parameter=gym.spaces.Box(low=np.array([0., 0., 0.], dtype=np.float32),
                                         high=np.array([3., 30., 30.], dtype=np.float32)),

            loss_coef=gym.spaces.Box(low=np.array([0., 0., 0., 0.], dtype=np.float32),
                                     high=np.array([150., 150., 9., 9.], dtype=np.float32)),

            loss_rad=gym.spaces.Box(low=np.array([0., 0., 0., 0., 0., 0., 0.], dtype=np.float32),
                                    high=np.array([0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6], dtype=np.float32)),
        )

        self.renderer = None

    def render(self):
        if self.renderer is None:
            self.renderer = MPCPlotter(
                direction_norm=0.5,
                local_map_size=self.controller.mpc.cost_map_region,
                global_map_size=self.state.config.global_map_size,
                local_map_scale=self.state.config.local_map_scale,
                global_map_scale=self.state.config.global_map_scale,
                wall_avoidance_points=self.controller.controller_config.wall_avoidance_points,
                max_step=self.sim.gym_config.max_steps
            )

        robot_pose = self.state.robot_pose

        self.renderer.update(
            mpc_fw_loss=self.controller.mpc.fw_loss,
            joint_angles=self.state.joint_angles,
            local_map=self.controller.local_map[:, :, :-1],
            global_map=self.state.global_map[:, :, 0],
            base_goal=self.controller.goto_goal,
            human_features=self.controller.human_features,
            pan_goal=self.controller.pan_goal,
            tangent_traj_vec=self.controller.tangent_traj_vec,
            target=None,
            waypoint=None,
            robot_pose=robot_pose
        )


    def reset(self):
        self.sim.reset()

        if self.controller is not None:
            self.controller.close()

        self.controller = RobotController(config_filename=self.config.controller_config_filename)

        self.state, _, _, _ = self.sim.step([0., 0., 0.])

        return self.get_obs(self.state)


    def step(self, action):
        reward = 0.0

        for mpc_step in range(self.config.n_mpc_steps):
            self.update_mpc(action)
            self.controller.step(self.state)
            u = self.controller.actions[0].tolist()
            self.state, _, done, info = self.sim.step(u)

        return self.get_obs(self.state), reward, done, info


    def get_obs(self, state):

        global_map = state.global_map

        obs = eu.AttrDict()

        obs.occupancy_map = global_map[:, :, 0].astype(bool)

        obs.human_position_map = np.zeros((*global_map.shape[:2],), dtype=bool)
        obs.human_orientation_map = np.zeros((*global_map.shape[:2],), dtype=np.float32)
        obs.human_linear_velocity_map = np.zeros((*global_map.shape[:2],), dtype=np.float32)
        obs.human_angular_velocity_map = np.zeros((*global_map.shape[:2],), dtype=np.float32)

        for i in range(state.humans_pose.shape[0]):
            x = state.humans_pose[i, 0]
            y = state.humans_pose[i, 1]
            ipx = int((self.sim.env_config.global_map_center[1] - y) * self.sim.env_config.global_map_scale)
            jpx = int((x - self.sim.env_config.global_map_center[0]) * self.sim.env_config.global_map_scale)

            obs.human_position_map[ipx, jpx] = 1
            obs.human_orientation_map[ipx, jpx] = state.humans_pose[i, 2]
            obs.human_linear_velocity_map[ipx, jpx] = state.humans_velocity[i, 0]
            obs.human_angular_velocity_map[ipx, jpx] = state.humans_velocity[i, 1]

        obs.robot_pose = np.copy(state.robot_pose)  # (x,y,theta)
        obs.joint_angles = np.copy(state.joint_angles)  # (pan)

        return obs


    def update_mpc(self, action):
        # controller in goto mode
        self.controller.mode = 0

        self.goto_goal = action['goto_goal']
        self.pan_goal = action['pan_goal']

        # Goals
        goto_local_goal = utils.global_to_local(self.state.robot_pose,
                np.array(self.goto_goal[:2]))
        goto_local_angle = self.goto_goal[2] - self.state.robot_pose[-1]

        self.controller.goto_goal[:2] = goto_local_goal
        self.controller.goto_goal[2] = utils.constraint_angle(goto_local_angle)
        self.controller.goto_goal[3:-1] = 0

        self.controller.goto_goal[-1] = 1  # enable
        self.controller.goto_goal[-2] = 1

        pan_local_goal = utils.global_to_local(self.state.robot_pose,
                np.array(self.pan_goal))

        self.controller.pan_goal[:2] = pan_local_goal
        self.controller.pan_goal[-1] = 1  # enable

        # Weights
        self.controller.loss_coef[:3] = action['loss_coef'][:3]
        self.controller.loss_coef[-1] = action['loss_coef'][-1]
        self.controller.loss_rad[:] = action['loss_rad']

        self.controller.weights[0] = action['weights'][0]
        self.controller.weights[2] = action['weights'][1]
        self.controller.weights[3] = action['weights'][2]

        self.controller.reg_parameter[:] = action['reg_parameter']


if __name__ == '__main__':

    # try to seed the sim and controller from outside, but without effect

    env = MpcRobotSimEnv(sim_gui=True)

    action = dict(goto_goal=[7.0, -3.0, 0.],
                  pan_goal=[0, 0],
                  weights=[1., 1000., 200.],
                  reg_parameter=[1., 10., 10.],
                  loss_coef=[50, 50, 3, 3],
                  loss_rad=[0.2, 0.2, 0.1, 0.2, 0.2, 0.2, 0.1])

    env.reset()
    for i in range(100):
        obs, reward, done, info = env.step(action)
        env.render()
    print(obs.robot_pose)

    env.reset()
    for i in range(100):
        obs, reward, done, info = env.step(action)
        env.render()
    print(obs.robot_pose)

    print('Finish.')
