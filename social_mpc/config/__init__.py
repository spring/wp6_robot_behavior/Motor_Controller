# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2022 by Inria
# Authors : Chris Reinke, Alex Auternaud, Timothée Wintz
# chris.reinke@inria.fr
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr