If video recorder => takes more time
                  => higher distance run
                  => lower fw_loss
                  => higher smoothness1 (dpan, omega, v)
                  => better smoothness2 (dpan, omega, v)

standard :
  visual : results better with h = 0.15
1st_action :
  visual : results a bit better with h = 0.20

max_iter_optim :
  better with 5/10/20 etc but too much time with > 10
  => Keep 5 or 10
  with 2, we don't really if standard or 1st_action is better
  but with 5/10, 1st_action much better than standard, in term of visual results
  (the robot doesn't go so far to go back) and total time, distance, fw_loss
  for h = 0.2 and 0.15, better with 5, 10 (in time loop, better with 5)
reg_parameter:
  best for h = 0.20 : [10., 10., 10.], [1., 1., 1.], [10., 1., 1.],
  [10., 10., 1.]
  but [10., 10., 1.] and [10., 10., 10.] have a pick in the loop time
  choose between   [1., 1., 1.] and  [10., 1., 1.] (best)
  same for h = 0.15 [10., 1., 1.] (best)
h :
  results are equals for h >= 0.12 (< 0.3, didn't try more than 0.3)
  expect for h = 0.12 and h = 0.15, lots of time overtaking,
  so h must be >= 0.17
threshold_map :
  any big differences, any value is better than no threshold. It seems like,
  less oscillations with 0.001

all results were confirmed with group1 and group0

--------------------------------------------------------------------------------

threshold_map :
  gr0 : 0.001 > 0.02 > 0.01
  gr1 : 0.001 > 0.02 > 0.01
  gr3 : 0.001 > 0.02 > 0.01

max_iter_optim :
  gr0 : 5 > 10
  gr1 : 5 > 10
  gr3 : 5 > 10

reg_parameter :
  gr0 : [10., 10., 10.] > [10., 1., 1.] > [1., 1., 1.]
  gr1 : [10., 10., 10.] > [1., 1., 1.] > [10., 1., 1.]
  gr3 h = 0.20 : [10., 10., 10.] > [1., 1., 1.] > [10., 1., 1.]
  gr3 h = 0.15 : [10., 10., 10.] > [10., 1., 1.] > [1., 1., 1.]

--------------------------------------------------------------------------------
